//
//  Event+extension.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 26/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

extension Event {
    @objc var sectionName: String? {
        
        return DateFormatter.dateFormatterMediumNone.string(from: self.date!)
        
    }
}
