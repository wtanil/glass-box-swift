//
//  Parameter+extension.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 21/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

extension Parameter {
    @objc var sectionName: String? {
        return DateFormatter.dateFormatterMediumNone.string(from: self.date!)
    }
}
