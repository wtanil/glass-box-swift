//
//  ParameterCreateController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 20/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ParameterCreateController: BaseCreateController {
    
    var form: ParameterForm
    
    let attributeTypes: [AttributeType] = AttributeType.allCases
    
    var attributeUnitString: [String] = []
    
    let types: [String] = AquariumType.allCases.map { $0.rawValue }
    
    init(tableView: UITableView, form: ParameterForm, sections: [FormSectionType], cellReuseIdentifiers: [String : String], formItems: [FormSectionType : [CreateFormItem]]) {
        
        self.form = form
        
        super.init(tableView: tableView, sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    func configureForm(with parameter: Parameter) {
        form.name = parameter.name!
        form.date = parameter.date!
        
        if let currentAttributes = parameter.parameterAttribute?.attributes {
            
            let convertedAttributes = currentAttributes as! Set<Attribute>
            
            for attribute in convertedAttributes {
                if let index = form.attributes.firstIndex(where: { $0.type == attribute.type }) {
                    form.attributes[index].active = true
                    form.attributes[index].value = attribute.value
                    form.attributes[index].unit = Int(attribute.unit)
                } else {
                    print("NOT FOUND")
                }
            }
        }
    }
    
    func configureDataSource(viewController: UIViewController, aquariumHandler: ((IndexPath) -> Void)?, unitHandler: (((AttributeType, Int, IndexPath)) -> Void)?) {
        
        weak var viewController = viewController
        
        dataSourceController.configureDataSource { [weak self] (tableView, indexPath, item) -> UITableViewCell? in
            
            guard let self = self else { return nil }
            
            let form = self.form
            switch item.type {
            case .name:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.eventCreateNameTVCell, for: indexPath) as! FormTextFieldTableViewCell
                
                cell.delegate = viewController as? FormTableViewCellProtocol
                cell.configureCell(with: form.name, placeholder: item.placeholder)
                
                return cell
            case .date:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.eventCreateDateTVCell, for: indexPath) as! FormDatePickerViewTableViewCell
                cell.delegate = viewController as? FormTableViewCellProtocol
                cell.configureCell(with: form.date, labelText: item.placeholder)
                
                return cell
                
            case .aquarium:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.eventCreateAquariumTVCell, for: indexPath) as! FormMultipleChoicesTableViewCell
                
                let aquariumNameArray = form.aquarium.map{ $0.name! }
                
                cell.configureCell(aquariums: aquariumNameArray, placeholder: item.placeholder) {
                    
                    if let aquariumHandler = aquariumHandler {
                        aquariumHandler(indexPath)
                    }
                }
                
                return cell
                
            case .attributes:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.parameterCreateAttributeTVCell, for: indexPath) as! FormSliderTableViewCell
                
                cell.delegate = viewController as? FormTableViewCellProtocol
                
                let attribute = form.attributes[indexPath.row]
                let attributeType = self.attributeTypes[Int(attribute.type)]
                guard let sliderConfig = SliderConfiguration.value[attributeType] else {
                    fatalError("Slider configuration doesn't exist")
                }
                let unitTitle = self.attributeUnitString[attribute.unit]
                
                cell.configureCell(with: item.placeholder,
                                   isActive: attribute.active,
                                   value: attribute.value,
                                   sliderConfig: sliderConfig,
                                   unit: attribute.unit,
                                   unitTitle: unitTitle) {
                                    if let unitHandler = unitHandler {
                                        unitHandler((attributeType, attribute.unit, indexPath))
                                    }
                }
                
                return cell
            default:
                return nil
                
            }
        }
    }
    
    func configureAttributes() {
        attributeUnitString = UnitType.allCases.map { $0.rawValue }
        var attributeArray: [CreateFormItem] = []
        
        for (index, type) in attributeTypes.enumerated() {
            
            // Get DEFAULT stored unit value
            let attributeName = type.rawValue
            let key = "\(Constant.unitKey)\(attributeName)"
            let storedUnit = UnitController().get(key: key)
            
            // Add and configure default parameter form attribute value
            form.attributes.append(ParameterForm.AttributeForm(type: Int16(index), unit: storedUnit))
            
            attributeArray.append(CreateFormItem(type: .attributes, placeholder: type.rawValue))
        }
        dataSourceController.formItems[FormSectionType.attributes] = attributeArray
    }
    
}

