//
//  HomeCreateController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 20/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class HomeCreateController: BaseHomeController {
    
    func configureDataSource() {
        dataSourceController.configureDataSource { (tableView, indexPath, item) -> UITableViewCell? in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.homeCreateTVCell, for: indexPath)
            cell.textLabel?.text = item.title
            return cell
            
        }
    }
    
    func didSelectRow(in viewController: UIViewController, at indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        unowned let viewController = viewController
        
        switch indexPath.item {
        case 0:
            viewController.performSegue(withIdentifier: SegueIdentifier.homeCreateToAquariumCreateSegue, sender: nil)
        case 1:
            viewController.performSegue(withIdentifier: SegueIdentifier.homeCreateToEventCreateSegue, sender: nil)
        case 2:
            viewController.performSegue(withIdentifier: SegueIdentifier.homeCreateToParameterCreateSegue, sender: nil)
        default:
            break
        }
    }
    

}

