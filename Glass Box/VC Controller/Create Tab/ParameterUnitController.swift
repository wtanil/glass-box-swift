//
//  ParameterUnitController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 28/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ParameterUnitController {
    
    var tableView: UITableView
    
    var dataSourceController: ParameterUnitFormDataSourceController!
    
    let unitTypes: [String] = UnitType.allCases.map { $0.rawValue }
    
    init(tableView: UITableView, sections: [FormSectionType], cellReuseIdentifiers: [String : String], formItems: [ParameterUnitFormItem]) {
        
        self.tableView = tableView
        
        self.dataSourceController = ParameterUnitFormDataSourceController(tableView: tableView, sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    func configureTableView() {
        dataSourceController.configureTableView()
        
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
    }
    
    func apply() {
        dataSourceController.apply()
    }
    
    func configureDataSource() {
        
        dataSourceController.configureDataSource { (tableView, indexPath, item) -> UITableViewCell? in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.parameterUnitTVCell, for: indexPath) as! FormMultipleSelectionTableViewCell
            
            var titleText = item.title
            if titleText.isEmpty {
                titleText = "None"
            }
            
            cell.configureCell(with: titleText)
            cell.selectionStyle = .none
            
            return cell
        }
        
    }
    
    func configureFormItem(attributeType: AttributeType) {
        guard let attributeUnitTypes = AttributeUnitType.value[attributeType] else {
            fatalError("Attribute type is not registered")
        }
        
        dataSourceController.formItems.removeAll()
        
        for index in attributeUnitTypes {
            let unitTitle: String = unitTypes[index]
            
            dataSourceController.formItems.append(ParameterUnitFormItem(title: unitTitle, value: index))
        }
    }
    
    func itemFor(indexPath: IndexPath) -> ParameterUnitFormItem? {
        return dataSourceController.dataSource.itemIdentifier(for: indexPath)
    }
    
    func willDisplay(forRowAt indexPath: IndexPath, selectedUnit: Int? ) {
        
        guard let item = itemFor(indexPath: indexPath) else {
            fatalError("Wrong index path for item")
        }
        
        guard let selectedUnit = selectedUnit else {
            fatalError("Selected unit is not valid")
            }
        
        if item.value == selectedUnit {
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
    }
    
    func didSelectRow(at indexPath: IndexPath) -> Int {
        
        guard let item = itemFor(indexPath: indexPath) else {
            fatalError("Wrong index path for item")
        }
        
        return item.value
        
    }
}

