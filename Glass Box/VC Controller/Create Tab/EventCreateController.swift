//
//  EventCreateController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 20/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class EventCreateController: BaseCreateController {
    
    var form: EventForm
    
    let typePickerViewDataSource: [String] = AquariumType.allCases.map { $0.rawValue }
    
    init(tableView: UITableView, form: EventForm, sections: [FormSectionType], cellReuseIdentifiers: [String : String], formItems: [FormSectionType : [CreateFormItem]]) {
        self.form = form
        
        super.init(tableView: tableView, sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    func configureForm(with event:
    Event) {
        form.name = event.name!
        form.date = event.date!
        form.notes = event.notes!
    }
    
    func configureDataSource(viewController: UIViewController, aquariumHandler: ((IndexPath) -> Void)?) {
        
        weak var viewController = viewController
        
        dataSourceController.configureDataSource { [weak self] (tableView, indexPath, item) -> UITableViewCell? in
            
            guard let self = self else {return nil}
            
            let form = self.form
            
            switch item.type {
            case .name:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.eventCreateNameTVCell, for: indexPath) as! FormTextFieldTableViewCell
                cell.delegate = viewController as? FormTableViewCellProtocol
                cell.configureCell(with: form.name, placeholder: item.placeholder)
                return cell
                
            case .date:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.eventCreateDateTVCell, for: indexPath) as! FormDatePickerViewTableViewCell
                cell.delegate = viewController as? FormTableViewCellProtocol
                cell.configureCell(with: form.date, labelText: item.placeholder)
                return cell
                
            case .notes:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.eventCreateNotesTVCell, for: indexPath) as! FormTextViewTableViewCell
                cell.delegate = viewController as? FormTableViewCellProtocol
                
                cell.configureCell(with: form.notes, placeholder: item.placeholder)
                return cell
                
            case .aquarium:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.eventCreateAquariumTVCell, for: indexPath) as! FormMultipleChoicesTableViewCell
                
                let aquariumNameArray = form.aquarium.map{ $0.name! }
                
                cell.configureCell(aquariums: aquariumNameArray, placeholder: item.placeholder) {
                    if let aquariumHandler = aquariumHandler {
                        aquariumHandler(indexPath)
                    }
                }
                return cell
            default: break
            }
            
            return nil
        }
        
    }
}
