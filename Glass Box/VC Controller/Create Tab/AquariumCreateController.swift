//
//  AquariumCreateController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 17/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class AquariumCreateController: BaseCreateController {
    
    var form: AquariumForm

    let typePickerViewDataSource: [String] = AquariumType.allCases.map { $0.rawValue }
    
    init(tableView: UITableView, form: AquariumForm, sections: [FormSectionType], cellReuseIdentifiers: [String : String], formItems: [FormSectionType : [CreateFormItem]]) {
        
        self.form = form
        
        super.init(tableView: tableView, sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
        
    }
    
    func configureForm(with aquarium: Aquarium) {
        form.name = aquarium.name!
        form.type = Int(aquarium.type)
        form.date = aquarium.date!
        form.synopsis = aquarium.synopsis!
    }
    
    func configureDataSource(viewController: UIViewController) {
        
        weak var viewController = viewController
        
        dataSourceController.configureDataSource { [weak self] (tableView, indexPath, item) -> UITableViewCell? in
            weak var viewController = viewController
            
            guard let self = self else { return nil }
            
            let form = self.form
            
            switch item.type {
            case .name:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumCreateNameTVCell, for: indexPath) as! FormTextFieldTableViewCell
                cell.delegate = viewController as? FormTableViewCellProtocol
                cell.configureCell(with: form.name, placeholder: item.placeholder)
                return cell
                
            case .type:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumCreateTypeTVCell, for: indexPath) as! FormPickerViewTableViewCell
                cell.delegate = viewController as? FormTableViewCellProtocol
                let typePickerViewDataSource = self.typePickerViewDataSource
                cell.configureCell(with: typePickerViewDataSource, row: form.type, labelText: item.placeholder)
                return cell
                
            case .date:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumCreateDateTVCell, for: indexPath) as! FormDatePickerViewTableViewCell
                cell.delegate = viewController as? FormTableViewCellProtocol
                cell.configureCell(with: form.date, labelText: item.placeholder)
                return cell
                
            case .synopsis:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumCreateSynopsisTVCell, for: indexPath) as! FormTextViewTableViewCell
                cell.delegate = viewController as? FormTableViewCellProtocol
                cell.configureCell(with: form.synopsis, placeholder: item.placeholder)
                return cell
                
            default:
                return nil
            }
        }
    }
}

