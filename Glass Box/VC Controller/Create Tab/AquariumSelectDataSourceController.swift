//
//  AquariumSelectDataSourceController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 27/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class AquariumSelectDataSourceController: HomeMainController {
    
    var selectedAquariums: [Aquarium]
    
    init(tableView: UITableView, fetchedResultsController: NSFetchedResultsController<Aquarium>, cellReuseIdentifiers: [String : String], selectedAquariums: [Aquarium]) {
        
        self.selectedAquariums = selectedAquariums
        
        super.init(tableView: tableView, fetchedResultsController: fetchedResultsController, cellReuseIdentifiers: cellReuseIdentifiers)
        
    }
    
    override func configureTableView() {
        super.configureTableView()
        tableView.allowsMultipleSelection = true
    }
    
    func willDisplay(forRowAt indexPath: IndexPath) {
        guard let aquarium = object(at: indexPath) else {
            fatalError("Attempt to configure cell without a managed object")
        }
        
        if selectedAquariums.contains(aquarium) {
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
    }
    
    func didSelectRowAt(_ indexPath: IndexPath) -> [Aquarium] {
        guard let aquarium = object(at: indexPath) else {
            fatalError("Attempt to configure cell without a managed object")
        }
        selectedAquariums.append(aquarium)
        return selectedAquariums
    }
    
    func didDeselectRowAt(_ indexPath: IndexPath) -> [Aquarium] {
        guard let aquarium = object(at: indexPath) else {
            fatalError("Attempt to configure cell without a managed object")
        }
        
        guard let index = selectedAquariums.firstIndex(of: aquarium) else {
            fatalError("Attemp to get an aquarium from empty array")
        }
        
        selectedAquariums.remove(at: index)
        return selectedAquariums
    }
    
    override func configureDataSource() {
        
        dataSourceController.configureDataSource { [weak self] (tableView, indexPath, objectId) -> UITableViewCell? in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumSelectTVCell, for: indexPath) as! FormMultipleSelectionTableViewCell
            
            guard let aquarium = self!.object(at: indexPath) else {
                fatalError("Attempt to configure cell without a managed object")
            }
            
            cell.configureCell(with: aquarium.name!)
            return cell
        }
    }
}
