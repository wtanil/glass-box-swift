//
//  AquariumShowController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 30/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import Charts


class AquariumShowController: BaseShowController {
    
    let aquariumTypes = AquariumType.allCases
    let attributeTypeString = AttributeType.allCases.map { $0.rawValue }
    let attributeUnit: [String] = UnitType.allCases.map { $0.rawValue }
    
    var sortKey: [String] = []
    var attributeData: [String: (Float, String, Date)?] = [:]
    
    var latestEvent: Event?
    
    var historyChartRawData: [Date: (Int, Int)] = [:]
    
    func configureDataSource(aquarium: Aquarium, eventEmptyAction: (() -> Void)?, eventExistAction: (() -> Void)?, parameterAction: (() -> Void)?) {
        
        dataSourceController.configureDataSource { [weak self] (tableView, indexPath, item) -> UITableViewCell? in
            
            guard let self = self else { return nil }
            
            switch item.type {
            case .name:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumShowNameTVCell, for: indexPath) as! ShowTitleTableViewCell
                
                cell.configureCell(with: aquarium.name!)
                
                return cell
                
            case .type:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumShowTypeTVCell, for: indexPath) as! ShowTypeTableViewCell
                
                let typeIndex = Int(aquarium.type)
                
                let typeString = AquariumType.allCases[typeIndex].rawValue
                cell.configureCell(with: typeString)
                
                return cell
            case .date:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumShowDateTVCell, for: indexPath) as! ShowDateTableViewCell
                let dateString = DateFormatter.dateFormatterMediumNone.string(from: aquarium.date!)
                cell.configureCell(with: "Since", date: dateString)
                
                return cell
                
            case .updateDate:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumShowDateTVCell, for: indexPath) as! ShowDateTableViewCell
                let dateString = DateFormatter.dateFormatterMediumShort.string(from: aquarium.updateDate!)
                cell.configureCell(with: "Last update on", date: dateString)
                
                return cell
                
            case .synopsis:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumShowSynopsisTVCell, for: indexPath) as! ShowSynopsisTableViewCell
                
                cell.configureCell(with: aquarium.synopsis!)
                
                if aquarium.synopsis!.isEmpty {
                    cell.isHidden = true
                }
                
                return cell
                
            case.event:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumShowEventTVCell, for: indexPath) as! ShowEventTableViewCell
                
                if let latestEvent = self.latestEvent {
                    let eventDate = DateFormatter.dateFormatterMediumShort.string(from: latestEvent.date!)
                    let eventString = "Last event: \(latestEvent.name!) on \(eventDate)"
                    
                    cell.configureCell(with: eventString, buttonLabel: "More events") {
                        if let eventExistAction = eventExistAction {
                            eventExistAction()
                        }
                    }
                } else {
                    let eventString = "Add a new event"
                    cell.configureCell(with: eventString, buttonLabel: "Add an event") {
                        if let eventEmptyAction = eventEmptyAction {
                            eventEmptyAction()
                        }
                    }
                }
                
                return cell
                
            case .parameter:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumShowParameterTVCell, for: indexPath) as! ShowAquariumParameterTableViewCell
                
                cell.configureCell(with: self.attributeData, sortKey: self.sortKey) {
                    if let parameterAction = parameterAction {
                        parameterAction()
                    }
                }
                
                return cell
            case .lineChart:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.aquariumShowLineChartTVCell, for: indexPath) as! ShowLineChartTableViewCell
                
                cell.configureCell(with: self.historyChartRawData)
                
                print("asd")

                
                return cell

            default:
                return nil
            }
        }
    }
    
    func configureLatestEvent(eventController: EventController, aquarium: Aquarium) {
        latestEvent = eventController.latestEventFor(aquarium: aquarium)
    }
    
    
    func configureLatestAttributes(aquariumController: AquariumController, aquarium: Aquarium) {
        
        let attributes = aquariumController.latestParameterAttributesFor(aquarium: aquarium)
        
        let aquariumType = aquariumTypes[Int(aquarium.type)]
        
        guard let sortKeyIndex = AquariumAttributeType.value[aquariumType] else {
            fatalError("Attribute types for aquarium doesn't exist")
        }
        
        sortKey = []
        for index in sortKeyIndex {
            sortKey.append(attributeTypeString[index])
        }
        
        attributeData.removeAll()
        for attribute in attributes {
            let key = attributeTypeString[Int(attribute.type)]
            let value = attribute.value
            let unit = attributeUnit[Int(attribute.unit)]
            let date = attribute.date!
            attributeData[key] = (value, unit, date)
        }
    }
    
    func configureHistoryChart(aquarium: Aquarium, eventController: EventController, parameterController: ParameterController, from: Date, to: Date) {
        
        // TODO: from before to validation
        
        // check number of days
//        let noOfDayBetweenDates = Calendar.current.dateComponents([.day], from: from, to: to).day!
        
        // TODO: date range validation
        
        // extract all dates between from and to dates
        var dates: [Date] = [from]
        var temporaryDate = from
        /*
        while temporaryDate <= to {
            let newDate = Calendar.current.date(byAdding: .day, value: 1, to: temporaryDate)
            dates.append(newDate!)
            temporaryDate = newDate!
        }
 */
        
        repeat {
            let newDate = Calendar.current.date(byAdding: .day, value: 1, to: temporaryDate)
            dates.append(newDate!)
            temporaryDate = newDate!
        } while temporaryDate < to
        
        // sort dates array just in case
        dates = dates.sorted()
        
//        var chartRawData: [Date: Int] = [:]
        var chartRawData: [Date: (Int, Int)] = [:]
        
        let parameters = parameterController.get(for: aquarium, from: from, to: to)
        let events = eventController.get(for: aquarium, from: from, to: to)
        
        for date in dates {
            let filteredEventCount = events.filter { (event) -> Bool in
                return Calendar.current.isDate(date, inSameDayAs: event.date!)
            }.count
            
            let filteredParameterCount = parameters.filter { (parameter) -> Bool in
                return Calendar.current.isDate(date, inSameDayAs: parameter.date!)
            }.count
            
            chartRawData[date] = (filteredEventCount, filteredParameterCount)
        }
        
        historyChartRawData = chartRawData
        
    }
}
