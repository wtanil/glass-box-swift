//
//  EventShowController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 05/05/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class EventShowController: BaseShowController {
    
    func configureDataSource(event: Event) {
        
        dataSourceController.configureDataSource { (tableView, indexPath, item) -> UITableViewCell? in
            
            switch item.type {
            case .name:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.eventShowNameTVCell, for: indexPath) as! ShowTitleTableViewCell
                
                cell.configureCell(with: event.name!)
                return cell
                
            case .date:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.eventShowDateTVCell, for: indexPath) as! ShowDateTableViewCell
                let dateString = DateFormatter.dateFormatterMediumNone.string(from: event.date!)
                cell.configureCell(with: "", date: dateString)
                return cell
                
            case .notes:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.eventShowNotesTVCell, for: indexPath) as! ShowSynopsisTableViewCell
                cell.configureCell(with: event.notes!)
                if event.notes!.isEmpty {
                    cell.isHidden = true
                }
                return cell
            default:
                return nil
            }
        }
    }
}
