//
//  ParameterShowController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 05/05/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ParameterShowController: BaseShowController {
    
    var sortKey: [String] = []
    var attributeData: [String: (Float, String, Date)?] = [:]
    
    func configureDataSource(parameter: Parameter) {
        
        dataSourceController.configureDataSource { [weak self] (tableView, indexPath, item) -> UITableViewCell? in
  
//            guard let parameterItem = self!.parameter else {
//                fatalError("Parameter is empty")
//            }
            
            switch item.type {
            case .name:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.parameterShowNameTVCell, for: indexPath) as! ShowTitleTableViewCell
                
                cell.configureCell(with: parameter.name!)
                return cell
                
            case .date:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.parameterShowDateTVCell, for: indexPath) as! ShowDateTableViewCell
                let dateString = DateFormatter.dateFormatterMediumNone.string(from: parameter.date!)
                cell.configureCell(with: "", date: dateString)
                return cell
                
            case .attribute:
                let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.parameterShowParameterTVCell, for: indexPath) as! ShowParameterTableViewCell
                
                cell.configureCell(with: self!.attributeData, sortKey: self!.sortKey)
                
                return cell
            default:
                return nil
            }
            
        }
        
    }
    
    func configureAttributeData(parameter: Parameter) {
        
        let attributeTypeString = AttributeType.allCases.map { $0.rawValue }
        let aquariumTypes = AquariumType.allCases
        
        guard let currentAquariumType = parameter.aquarium?.type else {
            fatalError("Aquarium type doesn't exist")
        }
        
        let aquariumType = aquariumTypes[Int(currentAquariumType)]
        
        guard let sortKeyIndex = AquariumAttributeType.value[aquariumType] else {
            fatalError("Attribute types for aquarium doesn't exist")
        }
        
        if sortKey.isEmpty {
            for index in sortKeyIndex {
                sortKey.append(attributeTypeString[index])
            }
        }
        
        let attributeUnit: [String] = UnitType.allCases.map { $0.rawValue }
        
        guard let parameterAttribute = parameter.parameterAttribute else {
            fatalError("ParameterAttribute doesn't exist")
        }
        
        guard let attributes = parameterAttribute.attributes else {
            fatalError("Attributes doesn't exist")
        }
        
        let convertedAttributes = attributes as! Set<Attribute>
        
        attributeData.removeAll()
        for attribute in convertedAttributes {
            let key = attributeTypeString[Int(attribute.type)]
            let value = attribute.value
            let unit = attributeUnit[Int(attribute.unit)]
            let date = attribute.date!
            attributeData[key] = (value, unit, date)
        }
        
    }
    
}
