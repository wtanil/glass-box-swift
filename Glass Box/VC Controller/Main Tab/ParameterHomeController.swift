//
//  ParameterHomeController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 27/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

import UIKit
import CoreData

class ParameterHomeController: BaseCoreDataController {
    
    var fetchedResultsController: NSFetchedResultsController<Parameter>
    
    init(tableView: UITableView, fetchedResultsController: NSFetchedResultsController<Parameter>, cellReuseIdentifiers: [String : String]) {
        
        self.fetchedResultsController = fetchedResultsController
        
        super.init(tableView: tableView, cellReuseIdentifiers: cellReuseIdentifiers)
        
    }
    
    func fetch() {
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print("Core Data on fetch request error: \(error.localizedDescription)")
        }
        
    }
    
    func configureDataSource() {
        dataSourceController.configureDataSource { [weak self] (tableView, indexPath, objectId) -> UITableViewCell? in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.parameterHomeTVCell, for: indexPath) as! ParameterHomeTableViewCell
            guard let parameter = self!.object(at: indexPath) else {
                fatalError("Attempt to configure cell without managed object")
            }
            
            cell.configureCell(with: parameter)
            return cell
            
        }
    }
    
    func object(at indexPath: IndexPath) -> Parameter? {
        return fetchedResultsController.object(at: indexPath)
    }
    
    
    func didSelectRow(in viewController: UIViewController, at indexPath: IndexPath) {
        unowned let viewController = viewController
        viewController.performSegue(withIdentifier: SegueIdentifier.parameterHomeToParameterShowSegue, sender: indexPath)
    }
    
}
