//
//  HomeMainController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 27/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class HomeMainController: BaseCoreDataController {
    
    var fetchedResultsController: NSFetchedResultsController<Aquarium>
    
    init(tableView: UITableView, fetchedResultsController: NSFetchedResultsController<Aquarium>, cellReuseIdentifiers: [String : String]) {
        
        self.fetchedResultsController = fetchedResultsController
        
        super.init(tableView: tableView, cellReuseIdentifiers: cellReuseIdentifiers)
        
    }
    
    func fetch() {
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print("Core Data on fetch request error: \(error.localizedDescription)")
        }
        
    }
    
    func configureDataSource() {
        dataSourceController.configureDataSource { [weak self] (tableView, indexPath, objectId) -> UITableViewCell? in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.homeMainTVCell, for: indexPath) as! HomeMainTableViewCell
            
            let aquarium = self!.fetchedResultsController.object(at: indexPath)
            
            cell.configureCell(with: aquarium)
            return cell
            
        }
    }
    
    func object(at indexPath: IndexPath) -> Aquarium? {
        return fetchedResultsController.object(at: indexPath)
    }
    
    func didSelectRow(in viewController: UIViewController, at indexPath: IndexPath) {
        
        unowned let viewController = viewController
        viewController.performSegue(withIdentifier: SegueIdentifier.homeMainToAquariumShowSegue, sender: indexPath)
    }
}
