//
//  EventHomeController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 27/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class EventHomeController: BaseCoreDataController {
    
    var fetchedResultsController: NSFetchedResultsController<Event>
    
    init(tableView: UITableView, fetchedResultsController: NSFetchedResultsController<Event>, cellReuseIdentifiers: [String : String]) {
        
        self.fetchedResultsController = fetchedResultsController
        
        super.init(tableView: tableView, cellReuseIdentifiers: cellReuseIdentifiers)
        
    }
    
    func fetch() {
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print("Core Data on fetch request error: \(error.localizedDescription)")
        }
        
    }
    
    func configureDataSource() {
        dataSourceController.configureDataSource { [weak self] (tableView, indexPath, objectId) -> UITableViewCell? in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.eventHomeTVCell, for: indexPath) as! EventHomeTableViewCell
            let event = self!.fetchedResultsController.object(at: indexPath)
            
            cell.configureCell(with: event)
            return cell
        }
    }
    
    func object(at indexPath: IndexPath) -> Event? {
        return fetchedResultsController.object(at: indexPath)
    }
    
    
    func didSelectRow(in viewController: UIViewController, at indexPath: IndexPath) {
        
        unowned let viewController = viewController
        viewController.performSegue(withIdentifier: SegueIdentifier.eventHomeToEventShowSegue, sender: indexPath)
    }
    
}
