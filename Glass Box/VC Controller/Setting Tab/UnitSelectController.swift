//
//  UnitSelectController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 29/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

class UnitSelectController: ParameterUnitController {
    
    override func configureTableView() {
        dataSourceController.configureTableView()
    }
    
    override func willDisplay(forRowAt indexPath: IndexPath, selectedUnit: Int?) {
        
        guard let item = itemFor(indexPath: indexPath) else {
                   fatalError("Wrong index path for item")
               }
        
        if item.value == selectedUnit {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
    
}
