//
//  UnitEditController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 29/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class UnitEditController {
    
    var tableView: UITableView
    
    var dataSourceController: UnitEditFormDataSourceController!
    
    let units: [String] = UnitType.allCases.map { $0.rawValue }
    
    init(tableView: UITableView, sections: [FormSectionType], cellReuseIdentifiers: [String : String], formItems: [UnitEditFormItem]) {
        
        self.tableView = tableView
        self.dataSourceController = UnitEditFormDataSourceController(tableView: tableView, sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    func configureTableView() {
        dataSourceController.configureTableView()
    }
    
    func apply() {
        dataSourceController.apply()
    }
    
    func configureDataSource() {
        
        dataSourceController.configureDataSource { [weak self] (tableView, indexPath, item) -> UITableViewCell? in
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.unitEditTVCell, for: indexPath) as! SettingUnitTableViewCell
            
            let unitString: String
            if item.value == 0 {
                unitString = "None"
            } else {
                unitString = self!.units[item.value]
            }
            
            cell.configureCell(with: item.title, unit: unitString)
            
            return cell
            
        }
        
    }
    
    func itemFor(indexPath: IndexPath) -> UnitEditFormItem? {
        return dataSourceController.dataSource.itemIdentifier(for: indexPath)
    }
    
    func configureFormData(unitController: UnitController) {
        let attributeType = AttributeType.allCases
        
        dataSourceController.formItems.removeAll()
        
        for attribute in attributeType {
            let attributeName = attribute.rawValue
            let key = "\(Constant.unitKey)\(attributeName)"
            let storedUnit = unitController.get(key: key)
            
            dataSourceController.formItems.append(UnitEditFormItem(type: attribute, title: attributeName, value: storedUnit))
        }
    }
    
    func didSelectRow(in viewController: UIViewController, at indexPath: IndexPath) {
        
        // Workaround to prevent first cell click doesn't trigger performSegue function
        tableView.deselectRow(at: indexPath, animated: false)
        
        viewController.performSegue(withIdentifier: SegueIdentifier.unitEditToUnitSelectSegue, sender: indexPath)
    }
    
    
    
}
