//
//  HomeSettingController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 27/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class HomeSettingController: BaseHomeController {
    
    func configureDataSource(viewController: UIViewController) {
        dataSourceController.configureDataSource { (tableView, indexPath, item) -> UITableViewCell? in
            
            let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIdentifier.homeSettingTVCell, for: indexPath)
            cell.textLabel?.text = item.title
            return cell
            
        }
    }
    
    func didSelectRow(in viewController: UIViewController, at indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        switch indexPath.item {
        case 0:
            viewController.performSegue(withIdentifier: SegueIdentifier.homeSettingToUnitEditSegue, sender: self)
        default:
            break
        }
    }
    

}


