//
//  BaseShowController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 04/05/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class BaseShowController {
    
    weak var tableView: UITableView!
    
    var dataSourceController: ShowFormDataSourceController!
    
    init(tableView: UITableView, sections: [FormSectionType], cellReuseIdentifiers: [String : String], formItems: [FormSectionType : [ShowFormItem]]) {
        self.tableView = tableView
        self.dataSourceController = ShowFormDataSourceController(tableView: tableView, sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    func configureTableView() {
        dataSourceController.configureTableView()
    }
    
    func apply() {
        dataSourceController.apply()
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func itemFor(indexPath: IndexPath) -> ShowFormItem? {
        return dataSourceController.dataSource.itemIdentifier(for: indexPath)
    }
    
}
