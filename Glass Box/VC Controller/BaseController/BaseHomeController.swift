//
//  BaseHomeController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 27/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class BaseHomeController {

    var tableView: UITableView
    
    var dataSourceController: HomeFormDataSourceController!
    
    init(tableView: UITableView, sections: [FormSectionType], cellReuseIdentifier: String, formItems: [HomeFormItem]) {
        self.tableView = tableView
        
        self.dataSourceController = HomeFormDataSourceController(tableView: tableView, sections: sections, cellReuseIdentifier: cellReuseIdentifier, formItems: formItems)
    }
    
    func configureTableView() {
        dataSourceController.configureTableView()
    }
    
    func apply() {
        dataSourceController.apply()
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
}
