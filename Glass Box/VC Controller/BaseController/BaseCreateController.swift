//
//  BaseCreateController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 27/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class BaseCreateController {
    
    var tableView: UITableView
    
    var dataSourceController: CreateFormDataSourceController!
    
    init(tableView: UITableView, sections: [FormSectionType], cellReuseIdentifiers: [String : String], formItems: [FormSectionType : [CreateFormItem]]) {
        self.tableView = tableView
        self.dataSourceController = CreateFormDataSourceController(tableView: tableView, sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    func configureTableView() {
        dataSourceController.configureTableView()
    }
    
    func apply() {
        dataSourceController.apply()
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func itemFor(indexPath: IndexPath) -> CreateFormItem? {
        return dataSourceController.dataSource.itemIdentifier(for: indexPath)
    }
    
}
