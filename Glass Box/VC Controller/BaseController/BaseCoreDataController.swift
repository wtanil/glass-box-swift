//
//  BaseCoreDataController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 27/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class BaseCoreDataController {
    
    var tableView: UITableView
    
    var dataSourceController: CoreDataDataSourceController!
    
    init(tableView: UITableView, cellReuseIdentifiers: [String : String]) {
        self.tableView = tableView
        
        self.dataSourceController = CoreDataDataSourceController(tableView: tableView, cellReuseIdentifiers: cellReuseIdentifiers)
    }
    
    func configureTableView() {
        dataSourceController.configureTableView()
    }
    
    func apply(snapshot: NSDiffableDataSourceSnapshot<String, NSManagedObjectID>, animatingDifferences: Bool) {
        dataSourceController.apply(snapshot: snapshot, animatingDifferences: animatingDifferences)
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    
}
