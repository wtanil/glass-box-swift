//
//  EventCreateTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 26/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class EventCreateTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    
//    lazy var eventController = EventController(context: context!)
    var controller: EventCreateController!
    
    let validator = EventFormValidator()
    
    let sections: [FormSectionType] = [.aquarium, .name, .date, .notes]
    
    let formItems: [FormSectionType: [CreateFormItem]] = [
        .aquarium: [CreateFormItem(type: .aquarium, placeholder: "Select an aquarium or multiple aquariums (Required)")],
        .name: [CreateFormItem(type: .name, placeholder: "Name (Required)")],
        .date: [CreateFormItem(type: .date, placeholder: "Date")],
        .notes: [CreateFormItem(type: .notes, placeholder: "Short Description (Optional)")]
    ]
    
    let cellReuseIdentifiers : [String: String] = [
        CellReuseIdentifier.eventCreateNameTVCell: NibName.formTextFieldTVCell,
        CellReuseIdentifier.eventCreateDateTVCell: NibName.formDatePickerViewTVCell,
        CellReuseIdentifier.eventCreateNotesTVCell: NibName.formTextViewTVCell,
        CellReuseIdentifier.eventCreateAquariumTVCell: NibName.formMultipleChoicesTVCell
    ]
    
    @IBAction func cancelBarButtonItemAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: SegueIdentifier.eventCreateToHomeCreateUnwindSegue, sender: self)
    }
    
    @IBOutlet weak var saveBarButtonItem: UIBarButtonItem!
    
    @IBAction func saveBarButtonItemAction(_ sender: UIBarButtonItem) {
        EventController(context: context!).create(with: controller.form)
        
        self.performSegue(withIdentifier: SegueIdentifier.eventCreateToHomeCreateUnwindSegue, sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        self.navigationItem.title = "New Event"
        
        configureController()
        
        controller.configureTableView()
        controller.configureDataSource(viewController: self) {[weak self] (indexPath) in
            guard let self = self else { return }
            self.performSegue(withIdentifier: SegueIdentifier.eventCreateToAquariumSelectSegue, sender: indexPath)
        }
        
        controller.apply()
    }
    
    func configureController() {
        controller = EventCreateController(tableView: tableView, form: EventForm(), sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.identifier {
        case SegueIdentifier.eventCreateToAquariumSelectSegue:
            guard let navigationController = segue.destination as? UINavigationController, let aquariumSelectTVC = navigationController.topViewController as? AquariumSelectTableViewController else {
                fatalError("Wrong segue for AquariumSelectTVC")
            }
            aquariumSelectTVC.context = context
            aquariumSelectTVC.selectedAquariums = controller.form.aquarium
            aquariumSelectTVC.unwindSegueIdentifier = SegueIdentifier.aquariumSelectToEventCreateUnwindSegue
            aquariumSelectTVC.indexPath = sender as? IndexPath
            
        default:
            break
        }
    }
    
    @IBAction func unwindToEventCreate(_ unwindSegue: UIStoryboardSegue) {
        //        let sourceViewController = unwindSegue.source
        // Use data from the view controller which initiated the unwind segue
        switch unwindSegue.identifier {
        case SegueIdentifier.aquariumSelectToEventCreateUnwindSegue:
            let aquariumSelectTVC = unwindSegue.source as! AquariumSelectTableViewController
            controller.form.aquarium = aquariumSelectTVC.selectedAquariums!
            let indexPath = aquariumSelectTVC.indexPath
            
            let (hasError, _) = validator.validate(aquariums: controller.form.aquarium)
            let errorableCell = tableView.cellForRow(at: indexPath!) as! FormErrorable
            errorableCell.showErrorUI(hasError)
            saveBarButtonItem.isEnabled = validator.validate(createForm: controller.form)
            
            controller.reloadData()
        default:
            break
        }
    }
}

// MARK: - Form Table View Cell Protocol

extension EventCreateTableViewController: FormTableViewCellProtocol {
    
    func valueChanged(in cell: UITableViewCell, with value: Any) {
        
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        guard let formItem = controller.itemFor(indexPath: indexPath) else { return }
        var hasError: (Bool, [String])?
        
        switch formItem.type{
        case .name:
            controller.form.name = value as! String
            hasError = validator.validate(name: controller.form.name)
        case .date:
            controller.form.date = value as! Date
//            hasError = validator.validate(date: controller.form.date)
        case .notes:
            controller.form.notes = value as! String
        default: break
        }
        
        if let hasError = hasError {
            let errorableCell = cell as! FormErrorable
            errorableCell.showErrorUI(hasError.0)
            saveBarButtonItem.isEnabled = validator.validate(createForm: controller.form)
        }
    }
}
