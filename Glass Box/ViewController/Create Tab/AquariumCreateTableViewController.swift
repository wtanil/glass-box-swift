//
//  AquariumCreateTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 04/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import UIKit
import CoreData

class AquariumCreateTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    
//    lazy var aquariumController = AquariumController(context: context!)
    var controller: AquariumCreateController!
    let validator = AquariumFormValidator()
    
    let sections: [FormSectionType] = [.name, .type, .date,. synopsis]
    
    let formItems: [FormSectionType: [CreateFormItem]] = [
        .name: [CreateFormItem(type: .name, placeholder: "Name (Required)")],
        .type: [CreateFormItem(type: .type, placeholder: "Type (Required)")],
        .date: [CreateFormItem(type: .date, placeholder: "Date")],
        .synopsis: [CreateFormItem(type: .synopsis, placeholder: "Short Description (Optional)")]
    ]
    
    let cellReuseIdentifiers : [String: String] = [
        CellReuseIdentifier.aquariumCreateNameTVCell: NibName.formTextFieldTVCell,
        CellReuseIdentifier.aquariumCreateTypeTVCell: NibName.formPickerViewTVCell,
        CellReuseIdentifier.aquariumCreateDateTVCell: NibName.formDatePickerViewTVCell,
        CellReuseIdentifier.aquariumCreateSynopsisTVCell: NibName.formTextViewTVCell
    ]
    
    @IBAction func cancelBarButtonItemAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: SegueIdentifier.aquariumCreateToHomeCreateUnwindSegue, sender: self)
    }
    
    @IBAction func saveBarButtonItemAction(_ sender: UIBarButtonItem) {
        AquariumController(context: context!).create(with: controller.form)
        self.performSegue(withIdentifier: SegueIdentifier.aquariumCreateToHomeCreateUnwindSegue, sender: self)
    }
    
    @IBOutlet weak var saveBarButtonItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        self.navigationItem.title = "New Aquarium"
        
        configureController()
        
        controller.configureTableView()
        controller.configureDataSource(viewController: self)
        
        controller.apply()
    }
    
    func configureController() {
        controller = AquariumCreateController(tableView: tableView, form: AquariumForm(), sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
}

// MARK: - Form Table View Cell Protocol

extension AquariumCreateTableViewController: FormTableViewCellProtocol {
    
    func valueChanged(in cell: UITableViewCell, with value: Any) {
        
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        guard let formItem = controller.itemFor(indexPath: indexPath) else { return }
        var hasError: (Bool, [String])?
        
        switch formItem.type {
        case .name:
            controller.form.name = value as! String
            hasError = validator.validate(name: controller.form.name)
        case .type:
            controller.form.type = value as! Int
        case .date:
            controller.form.date = value as! Date
            hasError = validator.validate(date: controller.form.date)
        case .synopsis:
            controller.form.synopsis = value as! String
        default: break
        }
        
        if let hasError = hasError {
            let errorableCell = cell as! FormErrorable
            errorableCell.showErrorUI(hasError.0)
            saveBarButtonItem.isEnabled = validator.validate(form: controller.form)
        }
        
    }
}
