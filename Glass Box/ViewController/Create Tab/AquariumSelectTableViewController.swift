//
//  AquariumSelectTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 03/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class AquariumSelectTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    var unwindSegueIdentifier: String?
    var indexPath: IndexPath?
    var selectedAquariums: [Aquarium]?
    
    var dataController: AquariumSelectDataSourceController!
    
    let cellReuseIdentifiers : [String: String] = [
        CellReuseIdentifier.aquariumSelectTVCell: NibName.formMultipleSelectionTVCell,
    ]
    
    @IBAction func doneButtonAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: unwindSegueIdentifier!, sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        // Check if unwind segue identifier injectionis successful
        if unwindSegueIdentifier == nil {
            fatalError("Unwind segue identifier is nil")
        }
        
        configureController()
        
        dataController.configureTableView()
        dataController.configureDataSource()
        dataController.fetch()
        
    }
    
    func configureController() {
        
        let fetchRequest: NSFetchRequest<Aquarium> = Aquarium.fetchRequest()
        let sortByDate = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [sortByDate]
        let fetchedResultsController = NSFetchedResultsController<Aquarium>(
            fetchRequest: fetchRequest,
            managedObjectContext: context!,
            sectionNameKeyPath: #keyPath(Aquarium.sectionName),
            cacheName: nil)
        fetchedResultsController.delegate = self
        
        dataController = AquariumSelectDataSourceController(tableView: tableView, fetchedResultsController: fetchedResultsController, cellReuseIdentifiers: cellReuseIdentifiers, selectedAquariums: selectedAquariums!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Fix wrong position of bar button item in iOS 13
        navigationController?.navigationBar.setNeedsLayout()
    }
    
    // MARK: - Table View Data Source
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        dataController.willDisplay(forRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedAquariums = dataController.didSelectRowAt(indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        selectedAquariums = dataController.didDeselectRowAt(indexPath)
    }
    
}

extension AquariumSelectTableViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeContentWith snapshot: NSDiffableDataSourceSnapshotReference) {
        
        print("AquariumSelect controller didChangeContentWith")
        dataController.apply(snapshot: snapshot as NSDiffableDataSourceSnapshot<String, NSManagedObjectID>, animatingDifferences: true)
        
    }
    
}
