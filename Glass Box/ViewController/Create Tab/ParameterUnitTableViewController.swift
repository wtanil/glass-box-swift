//
//  ParameterUnitTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 26/03/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ParameterUnitTableViewController: UITableViewController {
    
    var attributeType: AttributeType?
    var selectedUnit: Int?
    var indexPath: IndexPath?
    var unwindSegueIdentifier: String?
    
    var controller: ParameterUnitController!
    
    var sections: [FormSectionType] = [.main]
    
    var formItems: [ParameterUnitFormItem] = []
    
    let cellReuseIdentifiers : [String: String] = [
        CellReuseIdentifier.parameterUnitTVCell: NibName.formMultipleSelectionTVCell
    ]
    
    @IBAction func doneBarButtonItemAction(_ sender: Any) {
        print("save")
        self.performSegue(withIdentifier: unwindSegueIdentifier!, sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if attributeType == nil {
            fatalError("AttributeType is nil")
        }
        
        if selectedUnit == nil {
            fatalError("SelectedUnit is nil")
        }
        
        if indexPath == nil {
            fatalError("Index path is nil")
        }
        
        if unwindSegueIdentifier == nil {
            fatalError("Unwind segue identifer is nil")
        }
        
        configureController()
        
        controller.configureTableView()
        controller.configureDataSource()
        controller.configureFormItem(attributeType: attributeType!)
        
        controller.apply()
        
    }
    
    func configureController() {
        controller = ParameterUnitController(tableView: tableView, sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Fix wrong position of bar button item in iOS 13
        navigationController?.navigationBar.setNeedsLayout()
        
        controller.apply()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
}

// MARK: - Table view data source
extension ParameterUnitTableViewController {
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        controller.willDisplay(forRowAt: indexPath, selectedUnit: selectedUnit)
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedUnit = controller.didSelectRow(at: indexPath)
    }
}
