//
//  HomeCreateTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 29/11/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import UIKit
import CoreData

class HomeCreateTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    
    var controller: HomeCreateController!
    
    let sections: [FormSectionType] = [.main]
    
    let formItems: [HomeFormItem] = [
        HomeFormItem(title: "Add Aquarium"),
        HomeFormItem(title: "Add Event"),
        HomeFormItem(title: "Add Water Parameter")
    ]
    
    let cellReuseIdentifier : String = CellReuseIdentifier.homeCreateTVCell
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        configureController()
        
        controller.configureTableView()
        controller.configureDataSource()
        
        controller.apply()
        
        checkDefaultUnit()
        
    }
    
    func configureController() {
        controller = HomeCreateController(tableView: tableView, sections: sections, cellReuseIdentifier: cellReuseIdentifier, formItems: formItems)
    }
    
    func checkDefaultUnit() {
        if !UnitController().isDefaultSet() {
            let checkAction: (UIAlertAction) -> Void = { alertAction in
                self.tabBarController?.selectedIndex = 3
            }
            let alert: UIAlertController = UnitController().getAlert(checkAction: checkAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        controller.didSelectRow(in: self, at: indexPath)
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.identifier {
        case SegueIdentifier.homeCreateToAquariumCreateSegue:
            guard let navigationController = segue.destination as? UINavigationController, let aquariumCreateTVC = navigationController.topViewController as? AquariumCreateTableViewController else {
                fatalError("Wrong segue for AquariumCreateTVC")
            }
            aquariumCreateTVC.context = context

        case SegueIdentifier.homeCreateToEventCreateSegue:
            guard let navigationController = segue.destination as? UINavigationController, let eventCreateTVC = navigationController.topViewController as? EventCreateTableViewController else {
                fatalError("Wrong segue for EventCreateTVC")
            }
            eventCreateTVC.context = context

        case SegueIdentifier.homeCreateToParameterCreateSegue:
            guard let navigationController = segue.destination as? UINavigationController, let parameterCreateTVC = navigationController.topViewController as? ParameterCreateTableViewController else {
                fatalError("Wrong sgue for ParameterCreateTVC")
            }
            parameterCreateTVC.context = context

        default:
            break
        }
    }
    
    @IBAction func unwindToHomeCreate(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        print("Unwind Segue from \(sourceViewController)")
        // Use data from the view controller which initiated the unwind segue
    }
    

}
