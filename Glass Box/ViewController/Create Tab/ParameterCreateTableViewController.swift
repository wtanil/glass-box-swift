//
//  ParameterCreateTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 24/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class ParameterCreateTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    
    //    lazy var parameterController = ParameterController(context: context!)
    var controller: ParameterCreateController!
    let validator = ParameterFormValidator()
    
    let sections: [FormSectionType] = [.aquarium, .name, .date, .attributes]
    
    let formItems: [FormSectionType: [CreateFormItem]] = [
        .aquarium: [CreateFormItem(type: .aquarium, placeholder: "Select an aquarium or multiple aquarium (Required)")],
        .name: [CreateFormItem(type: .name, placeholder: "Name (Required)")],
        .date: [CreateFormItem(type: .date, placeholder: "Date")]
    ]
    
    let cellReuseIdentifiers : [String: String] = [
        CellReuseIdentifier.parameterCreateNameTVCell: NibName.formTextFieldTVCell,
        CellReuseIdentifier.parameterCreateDateTVCell: NibName.formDatePickerViewTVCell,
        CellReuseIdentifier.parameterCreateAquariumTVCell: NibName.formMultipleChoicesTVCell,
        CellReuseIdentifier.parameterCreateAttributeTVCell: NibName.formSliderTVCell
    ]
    
    @IBAction func cancelBarButtonItemAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier:SegueIdentifier.parameterCreateToHomeCreateUnwindSegue,sender: self)
    }
    
    @IBOutlet weak var saveBarButtonItem: UIBarButtonItem!
    
    @IBAction func saveBarButtonItemAction(_ sender: UIBarButtonItem) {
        ParameterController(context: context!).create(with: controller.form)
        self.performSegue(withIdentifier:SegueIdentifier.parameterCreateToHomeCreateUnwindSegue, sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        self.navigationItem.title = "New Parameter"
        
        configureController()
        
        controller.configureAttributes()
        controller.configureTableView()
        
        let aquariumHandler: ((IndexPath) -> Void)? = {
            [weak self] (indexPath) in
            guard let self = self else { return }
            self.performSegue(withIdentifier: SegueIdentifier.parameterCreateToAquariumSelectSegue, sender: indexPath)
        }
        let unitHandler: (((AttributeType, Int, IndexPath)) -> Void)? = {
            [weak self] (tupple: (AttributeType, Int, IndexPath)) in
            guard let self = self else { return }
            self.performSegue(withIdentifier: SegueIdentifier.parameterCreateToParameterUnitSegue, sender: (tupple.0, tupple.1, tupple.2))
        }
        controller.configureDataSource(viewController: self, aquariumHandler: aquariumHandler, unitHandler: unitHandler)
        
        controller.apply()
        
    }
    
    func configureController() {
        controller = ParameterCreateController(tableView: tableView, form: ParameterForm(), sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.identifier {
        case SegueIdentifier.parameterCreateToAquariumSelectSegue:
            guard let navigationController = segue.destination as? UINavigationController, let aquariumSelectTVC = navigationController.topViewController as? AquariumSelectTableViewController else {
                fatalError("Wrong segue for AquariumSelectTVC")
            }
            aquariumSelectTVC.context = context
            aquariumSelectTVC.selectedAquariums = controller.form.aquarium
            aquariumSelectTVC.unwindSegueIdentifier = SegueIdentifier.aquariumSelectToParameterCreateUnwindSegue
            aquariumSelectTVC.indexPath = sender as? IndexPath
            
        case SegueIdentifier.parameterCreateToParameterUnitSegue:
            guard let navigationController = segue.destination as? UINavigationController, let parameterUnitTVC = navigationController.topViewController as? ParameterUnitTableViewController else {
                fatalError("Wrong segue for ParameterUnitTVC")
            }
            let data = sender as! (AttributeType, Int, IndexPath)
            parameterUnitTVC.attributeType = data.0
            parameterUnitTVC.selectedUnit = data.1
            parameterUnitTVC.indexPath = data.2
            parameterUnitTVC.unwindSegueIdentifier = SegueIdentifier.parameterUnitToParameterCreateUnwindSegue
            
        default:
            break
        }
    }
    
    @IBAction func unwindToParameterCreate(_ unwindSegue: UIStoryboardSegue) {
        //        let sourceViewController = unwindSegue.source
        // Use data from the view controller which initiated the unwind segue
        
        switch unwindSegue.identifier {
        case SegueIdentifier.aquariumSelectToParameterCreateUnwindSegue:
            let aquariumSelectTVC = unwindSegue.source as! AquariumSelectTableViewController
            controller.form.aquarium = aquariumSelectTVC.selectedAquariums!
            let indexPath = aquariumSelectTVC.indexPath
            
            let (hasError, _) = validator.validate(aquariums: controller.form.aquarium)
            let errorableCell = tableView.cellForRow(at: indexPath!) as! FormErrorable
            errorableCell.showErrorUI(hasError)
            saveBarButtonItem.isEnabled = validator.validate(createForm: controller.form)
            
        case SegueIdentifier.parameterUnitToParameterCreateUnwindSegue:
            let parameterUnitTVC = unwindSegue.source as! ParameterUnitTableViewController
            let selectedUnit = parameterUnitTVC.selectedUnit!
            let indexPath = parameterUnitTVC.indexPath!
            
            controller.form.attributes[indexPath.row].unit = selectedUnit
            
        default:
            break
        }
        
        controller.reloadData()
    }
}

// MARK: - Form Table View Cell Protocol

extension ParameterCreateTableViewController: FormTableViewCellProtocol {
    
    func valueChanged(in cell: UITableViewCell, with value: Any) {
        
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        guard let formItem = controller.itemFor(indexPath: indexPath) else { return }
        var hasError: (Bool, [String])?
        
        switch formItem.type{
        case .name:
            controller.form.name = value as! String
            hasError = validator.validate(name: controller.form.name)
        case .date:
            controller.form.date = value as! Date
        case .aquarium:
            controller.form.aquarium = value as! [Aquarium]
        case .attributes:
            let cellValue = value as! FormSliderTableViewCell.CellValue
            controller.form.attributes[indexPath.row].active = cellValue.isActive
            controller.form.attributes[indexPath.row].value = cellValue.value
            controller.form.attributes[indexPath.row].unit = cellValue.unit
        default: break
        }
        
        if let hasError = hasError {
            let errorableCell = cell as! FormErrorable
            errorableCell.showErrorUI(hasError.0)
            saveBarButtonItem.isEnabled = validator.validate(createForm: controller.form)
        }
    }
}
