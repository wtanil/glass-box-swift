//
//  UnitEditTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 19/03/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class UnitEditTableViewController: UITableViewController {
    
    var unitController = UnitController()
    
    var controller: UnitEditController!

    var section: [FormSectionType] = [.main]
    
    var formItems: [UnitEditFormItem] = []
    
    let cellReuseIdentifiers : [String: String] = [
        CellReuseIdentifier.unitEditTVCell: NibName.settingUnitTVC
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
        controller.configureTableView()
        controller.configureDataSource()
        
    }
    
    func configureController() {
        controller = UnitEditController(tableView: tableView, sections: section, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        controller.configureFormData(unitController: unitController)
        controller.apply()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        switch segue.identifier {
        case SegueIdentifier.unitEditToUnitSelectSegue:
            
            guard let item = controller.itemFor(indexPath: sender as! IndexPath) else {
                fatalError("Wrong index path for selected item")
            }
            
            guard let unitSelectTVC = segue.destination as? UnitSelectTableViewController else {
                fatalError("Wrong segue for UnitSelectTVC")
            }
            unitSelectTVC.attributeType = item.type
            
        default:
            break
        }
    }
}

// MARK: - Table View Data Source and Delegate
extension UnitEditTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        controller.didSelectRow(in: self, at: indexPath)
    }
    
}

