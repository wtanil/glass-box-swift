//
//  UnitSelectTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 23/03/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class UnitSelectTableViewController: UITableViewController {
    
    var attributeType: AttributeType?
    
    var controller: UnitSelectController!
    var unitController = UnitController()
    
    var key: String?
    
    var section: [FormSectionType] = [.main]
    
    var formItems: [ParameterUnitFormItem] = []
    
    let cellReuseIdentifiers: [String: String] = [
        CellReuseIdentifier.parameterUnitTVCell: NibName.formMultipleSelectionTVCell
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if type injection is successful
        if attributeType == nil {
            fatalError("AttributeType is nil")
        }
        
        print(attributeType!.rawValue)

        configureController()
        configureKey()
        
        controller.configureTableView()
        controller.configureDataSource()
    }
    
    func configureController() {
        controller = UnitSelectController(tableView: tableView, sections: section, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    func configureKey() {
        key = unitController.keyFor(attributeName: attributeType!.rawValue)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        controller.configureFormItem(attributeType: attributeType!)
        controller.apply()
        
    }
    
}

// MARK: - Table view data source
extension UnitSelectTableViewController {
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let selectedUnitIndex = unitController.get(key: key!)
        
        controller.willDisplay(forRowAt: indexPath, selectedUnit: selectedUnitIndex)
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let item = controller.itemFor(indexPath: indexPath) else {
            fatalError("Wrong index path for item")
        }
        
        unitController.set(key: key!, with: item.value)
    }
}
