//
//  HomeSettingTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 19/03/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class HomeSettingTableViewController: UITableViewController {
    
    var controller: HomeSettingController!
    
    let sections: [FormSectionType] = [.main]
    
    let formItems: [HomeFormItem] = [
        HomeFormItem(title: "Manage parameter attribute unit")
    ]
    
    let cellReuseIdentifier : String = CellReuseIdentifier.homeSettingTVCell
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
        
        controller.configureTableView()
        controller.configureDataSource(viewController: self)
        
        controller.apply()
    }
    
    func configureController() {
        controller = HomeSettingController(tableView: tableView, sections: sections, cellReuseIdentifier: cellReuseIdentifier, formItems: formItems)
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        controller.didSelectRow(in: self, at: indexPath)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     
     }
     */
    
    @IBAction func unwindToHomeSetting(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        print("Unwind Segue from \(sourceViewController)")
        // Use data from the view controller which initiated the unwind segue
    }
}
