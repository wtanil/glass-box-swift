//
//  AquariumEditTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 20/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class AquariumEditTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    var aquarium: Aquarium?
    
    lazy var aquariumController = AquariumController(context: context!)
    var controller: AquariumCreateController!
    let validator = AquariumFormValidator()
    
    let sections: [FormSectionType] = [.name, .date, .synopsis]
    
    let formItems: [FormSectionType: [CreateFormItem]] = [
        .name: [CreateFormItem(type: .name, placeholder: "Name (Required)")],
        .date: [CreateFormItem(type: .date, placeholder: "Date")],
        .synopsis: [CreateFormItem(type: .synopsis, placeholder: "Short Description (Optional)")]
    ]
    
    let cellReuseIdentifiers: [String: String] = [
        CellReuseIdentifier.aquariumCreateNameTVCell: NibName.formTextFieldTVCell,
        CellReuseIdentifier.aquariumCreateDateTVCell: NibName.formDatePickerViewTVCell,
        CellReuseIdentifier.aquariumCreateSynopsisTVCell: NibName.formTextViewTVCell
    ]
    
    @IBAction func cancelBarButtonItemAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: SegueIdentifier.aquariumEditToAquariumShowUnwindSegue, sender: self)
    }
    
    @IBAction func saveBarButtonItemAction(_ sender: UIBarButtonItem) {
        
        aquariumController.update(aquarium: aquarium!, with: controller.form)
        
        self.performSegue(withIdentifier: SegueIdentifier.aquariumEditToAquariumShowUnwindSegue, sender: self)
    }
    
    @IBOutlet weak var saveBarButtonItem: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        // Check if aquarium injection is successful
        if aquarium == nil {
            fatalError("Aquarium is nil")
        }
        
        configureController()
        
        controller.configureForm(with: aquarium!)
        controller.configureTableView()
        controller.configureDataSource(viewController: self)
        
        controller.apply()
        
    }
    
    func configureController() {
        controller = AquariumCreateController(tableView: tableView, form: AquariumForm(), sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
}

// MARK: - Form Table View Cell Protocol

extension AquariumEditTableViewController: FormTableViewCellProtocol {
    
    func valueChanged(in cell: UITableViewCell, with value: Any) {
        
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        guard let formItem = controller.itemFor(indexPath: indexPath) else { return }
        var hasError: (Bool, [String])?
        
        switch formItem.type{
        case .name:
            controller.form.name = value as! String
            hasError = validator.validate(name: controller.form.name)
        case .date:
            controller.form.date = value as! Date
            hasError = validator.validate(date: controller.form.date)
        case .synopsis:
            controller.form.synopsis = value as! String
        default: break
        }
        
        if let hasError = hasError {
            let errorableCell = cell as! FormErrorable
            errorableCell.showErrorUI(hasError.0)
            saveBarButtonItem.isEnabled = validator.validate(form: controller.form)
        }
    }
 
}
