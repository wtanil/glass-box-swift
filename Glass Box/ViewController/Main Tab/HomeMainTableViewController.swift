//
//  HomeMainTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 02/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import UIKit
import CoreData

class HomeMainTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    
    var dataController: HomeMainController!
    
    let cellReuseIdentifiers : [String: String] = [
        CellReuseIdentifier.homeMainTVCell: NibName.homeMainTVCell
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        configureController()
        
        dataController.configureTableView()
        dataController.configureDataSource()
        dataController.fetch()
        
    }
    
    func configureController() {
        
        let fetchRequest: NSFetchRequest<Aquarium> = Aquarium.fetchRequest()
        let sortByDate = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [sortByDate]
        let fetchedResultsController = NSFetchedResultsController<Aquarium>(
            fetchRequest: fetchRequest,
            managedObjectContext: context!,
            sectionNameKeyPath: #keyPath(Aquarium.sectionName),
            cacheName: nil)
        fetchedResultsController.delegate = self
        
        dataController = HomeMainController(tableView: tableView, fetchedResultsController: fetchedResultsController, cellReuseIdentifiers: cellReuseIdentifiers)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataController.reloadData()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        switch segue.identifier {
            
        case SegueIdentifier.homeMainToAquariumShowSegue:
            guard let aquariumShowTVC = segue.destination as? AquariumShowTableViewController else {
                fatalError("Wrong segue for AquariumShowTVC")
            }
            aquariumShowTVC.context = context
            
            if let aquarium = dataController.object(at: sender as! IndexPath) {
                aquariumShowTVC.aquarium = aquarium
            }
            break
        default:
            break
            
        }
    }
    
    @IBAction func unwindToHomeMain(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        print("Unwind Segue from \(sourceViewController)")
        // Use data from the view controller which initiated the unwind segue
    }
}

// MARK: - Table view data source
extension HomeMainTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dataController.didSelectRow(in: self, at: indexPath)
    }
}

extension HomeMainTableViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeContentWith snapshot: NSDiffableDataSourceSnapshotReference) {
        
        print("HomeMainTVC controller didChangeContentWith")
        dataController.apply(snapshot: snapshot as NSDiffableDataSourceSnapshot<String, NSManagedObjectID>, animatingDifferences: true)
        
    }
    
}

