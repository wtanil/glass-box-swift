//
//  EventHomeTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 22/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class EventHomeTableViewController: UITableViewController {

    var context: NSManagedObjectContext?
    var aquarium: Aquarium?
    
    var dataController: EventHomeController!

    let cellReuseIdentifiers : [String: String] = [
        CellReuseIdentifier.eventHomeTVCell: NibName.eventHomeTVCell
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        // Check if aquarium injection is successful
        if aquarium == nil {
            fatalError("Aquarium is nil")
        }
        
        configureController()
        
        dataController.configureTableView()
        dataController.configureDataSource()
        dataController.fetch()
    }
    
    func configureController() {
        
        let fetchRequest: NSFetchRequest<Event> = Event.fetchRequest()
        let sortByDate = NSSortDescriptor(key: "date", ascending: false)
        fetchRequest.sortDescriptors = [sortByDate]
        fetchRequest.predicate = NSPredicate(format: "aquarium = %@", aquarium!)
        let fetchedResultsController = NSFetchedResultsController<Event>(
            fetchRequest: fetchRequest,
            managedObjectContext: context!,
            sectionNameKeyPath: #keyPath(Event.sectionName),
            cacheName: nil)
        fetchedResultsController.delegate = self
        
        dataController = EventHomeController(tableView: tableView, fetchedResultsController: fetchedResultsController, cellReuseIdentifiers: cellReuseIdentifiers)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataController.reloadData()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.identifier {
        case SegueIdentifier.eventHomeToEventShowSegue:
            
            guard let eventShowTVC = segue.destination as? EventShowTableViewController else {
                fatalError("Wrong segue for EventShowTVC")
            }
            
            eventShowTVC.context = context
            
            if let event = dataController.object(at: sender as! IndexPath) {
                eventShowTVC.event = event
            }
            
        default:
            break
            
        }
    }
    
    @IBAction func unwindToEventHome(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        print("Unwind Segue from \(sourceViewController)")
        // Use data from the view controller which initiated the unwind segue
        
    }
}

// MARK: - Table view data source
extension EventHomeTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dataController.didSelectRow(in: self, at: indexPath)
    }
}

extension EventHomeTableViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeContentWith snapshot: NSDiffableDataSourceSnapshotReference) {
        
        print("EventHomeTVC controller didChangeContentWith")

        dataController.apply(snapshot: snapshot as NSDiffableDataSourceSnapshot<String, NSManagedObjectID>, animatingDifferences: true)
        
    }
    
}
