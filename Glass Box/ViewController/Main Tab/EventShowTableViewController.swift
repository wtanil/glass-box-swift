//
//  EventShowTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 18/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class EventShowTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    var event: Event?
    lazy var eventController = EventController(context: context!)
    
    var controller: EventShowController!
    
    let sections: [FormSectionType] = [.name, .date, .notes]
    
    let formItems: [FormSectionType: [ShowFormItem]] = [
        .name: [ShowFormItem(type: .name)],
        .date: [ShowFormItem(type: .date)],
        .notes: [ShowFormItem(type: .notes)]
    ]
    
    let cellReuseIdentifiers: [String: String] = [
        CellReuseIdentifier.eventShowNameTVCell: NibName.showTitleTVCell,
        CellReuseIdentifier.eventShowDateTVCell: NibName.showDateTVCell,
        CellReuseIdentifier.eventShowNotesTVCell: NibName.showSynopsisTVCell
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        // Check if event injection is successful
        if event == nil {
            fatalError("Event is nil")
        }
        
        configureController()
        configureRightBarButtonItem()
        
        controller.configureTableView()
        controller.configureDataSource(event: event!)
        controller.apply()
    }
    
    func configureController() {
        controller = EventShowController(tableView: tableView, sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    func configureRightBarButtonItem() {
        let editEventBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(self.editEventBarButtonItemAction(_:)))
        let deleteEventBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(self.deleteEventBarButtonItemAction(_:)))
        deleteEventBarButtonItem.tintColor = UIColor.systemRed
        
        self.navigationItem.rightBarButtonItems = [editEventBarButtonItem, deleteEventBarButtonItem]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        controller.reloadData()
    }
    
    @objc func editEventBarButtonItemAction(_ sender: Any) {
        
        self.performSegue(withIdentifier: SegueIdentifier.eventShowToEventEditSegue, sender: self)
    }
    
    @objc func deleteEventBarButtonItemAction(_ sender: Any) {
        
        let deleteHandler: (UIAlertAction) -> Void = {
            [weak self] alertAction in
            guard let self = self else { return }
            
            self.eventController.delete(event: self.event!)
            self.performSegue(withIdentifier: SegueIdentifier.eventShowToEventHomeUnwindSegue, sender: self)
        }
        
        let cancelHandler: (UIAlertAction) -> Void = { alertAction in
            print("The \"Cancel\" alert occured.")
        }
        
        let alert = AlertController().deleteAlert(
            with: "Delete",
            message: "Do you want to delete \(event!.name!)",
            deleteHandler: deleteHandler,
            cancelHandler: cancelHandler)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.identifier {
        case SegueIdentifier.eventShowToEventEditSegue:
            
            guard let navigationController = segue.destination as? UINavigationController, let eventEditTVC = navigationController.topViewController as? EventEditTableViewController else {
                fatalError("Wrong segue for EventEditTVC")
            }
            eventEditTVC.context = context
            eventEditTVC.event = event
            
            break
        default:
            break
        }
        
    }
    
    
    @IBAction func unwindToEventShow(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        print("Unwind Segue from \(sourceViewController)")
        // Use data from the view controller which initiated the unwind segue
        
        context?.refresh(event!, mergeChanges: false)
        controller.apply()
        controller.reloadData()
    }
    
}
