//
//  AquariumShowTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 15/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData
import Charts

class AquariumShowTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    var aquarium: Aquarium?
    var controller: AquariumShowController!
    
    let sections: [FormSectionType] = [.name, .type, .date, .updateDate, .synopsis, .lineChart, .event, .parameter]
    
    let cellReuseIdentifiers: [String: String] = [
        CellReuseIdentifier.aquariumShowNameTVCell: NibName.showTitleTVCell,
        CellReuseIdentifier.aquariumShowSynopsisTVCell: NibName.showSynopsisTVCell,
        CellReuseIdentifier.aquariumShowDateTVCell: NibName.showDateTVCell,
        CellReuseIdentifier.aquariumShowTypeTVCell: NibName.showTypeTVCell,
        CellReuseIdentifier.aquariumShowEventTVCell: NibName.showEventTVCell,
        CellReuseIdentifier.aquariumShowParameterTVCell: NibName.showAquariumParameterTVCell,
        CellReuseIdentifier.aquariumShowLineChartTVCell: NibName.showLineChartTVCell
    ]
    
    var formItems: [FormSectionType: [ShowFormItem]] = [
        .name: [ShowFormItem(type: .name)],
        .type: [ShowFormItem(type: .type)],
        .date: [ShowFormItem(type: .date)],
        .updateDate: [ShowFormItem(type: .updateDate)],
        .synopsis: [ShowFormItem(type: .synopsis)],
        .lineChart: [ShowFormItem(type: .lineChart)],
        .event: [ShowFormItem(type: .event)],
        .parameter: [ShowFormItem(type: .parameter)]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        // Check if aquarium injection is successful
        if aquarium == nil {
            fatalError("Aquarium is nil")
        }
        
        print("viewdidload")
        
        configureController()
        
        configureRightBarButtonItem()
        controller.configureTableView()
        
        // [weak self] to prevent memory leak
        let eventEmptyAction: (() -> ())? = { [weak self] () in
            guard let self = self else { return }
            self.tabBarController?.selectedIndex = 0
        }
        let eventExistAction: (() -> Void)? = {[weak self] () in
            guard let self = self else { return }
            self.performSegue(withIdentifier: SegueIdentifier.aquariumShowToEventHomeSegue, sender: nil)
        }
        let parameterAction: (() -> ())? = {
            [weak self ]() in
            guard let self = self else { return }
            self.performSegue(withIdentifier: SegueIdentifier.aquariumShowToParameterHomeSegue, sender: nil)
        }

        controller.configureDataSource(aquarium: aquarium!, eventEmptyAction: eventEmptyAction, eventExistAction: eventExistAction, parameterAction: parameterAction)
        
        
        let currentDate = Date()
        let startDate: Date = currentDate.addingTimeInterval(-6*24*60*60)
        let endDate: Date = currentDate
        
        controller.configureHistoryChart(aquarium: aquarium!, eventController: EventController(context: context!), parameterController: ParameterController(context: context!), from: startDate, to: endDate)
        
        
        controller.apply()
        
    }
    
    func configureController() {
        controller = AquariumShowController(tableView: tableView, sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewwillappear reloadData")
        
        controller.configureLatestEvent(eventController: EventController(context: context!), aquarium: aquarium!)
        controller.configureLatestAttributes(aquariumController: AquariumController(context: context!), aquarium: aquarium!)
        
        let currentDate = Date()
        let startDate: Date = currentDate.addingTimeInterval(-6*24*60*60)
        let endDate: Date = currentDate
        
        controller.configureHistoryChart(aquarium: aquarium!, eventController: EventController(context: context!), parameterController: ParameterController(context: context!), from: startDate, to: endDate)
        controller.reloadData()
        controller.apply()
    }
    
    @objc func editAquariumBarButtonItemAction(_ sender: Any) {
        
        self.performSegue(withIdentifier: SegueIdentifier.aquariumShowToAquariumEditSegue, sender: nil)
    }
    
    @objc func deleteAquariumBarButtonItemAction(_ sender: Any) {
        
        // [weak self] to prevent memory leak
        let deleteHandler: (UIAlertAction) -> Void = {
            [weak self] alertAction in
            
            guard let self = self else { return }
            
            AquariumController(context: self.context!).delete(aquarium: self.aquarium!)
            self.performSegue(withIdentifier: SegueIdentifier.aquariumShowToHomeMainUnwindSegue, sender: nil)
        }
        
        let cancelHandler: (UIAlertAction) -> Void = { alertAction in
            print("The \"Cancel\" alert occured.")
        }
        
        let alert = AlertController().deleteAlert(
            with: "Delete",
            message: "Do you want to delete \(aquarium!.name!)",
            deleteHandler: deleteHandler,
            cancelHandler: cancelHandler)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func configureRightBarButtonItem() {
        
        let editAquariumBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(self.editAquariumBarButtonItemAction(_:)))
        let deleteAquariumBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(self.deleteAquariumBarButtonItemAction(_:)))
        deleteAquariumBarButtonItem.tintColor = UIColor.systemRed
        
        self.navigationItem.rightBarButtonItems = [editAquariumBarButtonItem, deleteAquariumBarButtonItem]
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.identifier {
        case SegueIdentifier.aquariumShowToAquariumEditSegue:
            
            guard let navigationController = segue.destination as? UINavigationController, let aquariumEditTVC = navigationController.topViewController as? AquariumEditTableViewController else {
                fatalError("Wrong segue for AquariumEditTVC")
            }
            aquariumEditTVC.context = context
            aquariumEditTVC.aquarium = aquarium
            
        case SegueIdentifier.aquariumShowToEventHomeSegue:
            
            guard let eventHomeTVC = segue.destination as? EventHomeTableViewController else {
                fatalError("Wrong segue for EventHomeTVC")
            }
            eventHomeTVC.context = context
            eventHomeTVC.aquarium = aquarium
            
        case SegueIdentifier.aquariumShowToParameterHomeSegue:
            
            guard let parameterHomeTVC = segue.destination as? ParameterHomeTableViewController else {
                fatalError("Wrong segue for ParameterHomeTVC")
            }
            parameterHomeTVC.context = context
            parameterHomeTVC.aquarium = aquarium
            
        default: break
        }
    }
    
    @IBAction func unwindToAquariumShow(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        print("Unwind Segue from \(sourceViewController)")
        // Use data from the view controller which initiated the unwind segue
        
        context?.refresh(aquarium!, mergeChanges: false)
        controller.apply()
        controller.reloadData()
    }
    
}
