//
//  ParameterShowTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 09/03/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class ParameterShowTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    var parameter: Parameter?
    
    lazy var parameterController = ParameterController(context: context!)
    
    var controller: ParameterShowController!
    
    let sections: [FormSectionType] = [.name, .date, .attributes]
    
    var formItems: [FormSectionType: [ShowFormItem]] = [
        .name: [ShowFormItem(type: .name)],
        .date: [ShowFormItem(type: .date)],
        .attributes: [ShowFormItem(type: .attribute)]
    ]
    
    let cellReuseIdentifiers: [String: String] = [
        CellReuseIdentifier.parameterShowNameTVCell: NibName.showTitleTVCell,
        CellReuseIdentifier.parameterShowDateTVCell: NibName.showDateTVCell,
        CellReuseIdentifier.parameterShowParameterTVCell: NibName.showParameterTVCell
    ]
    
    @objc func editParameterBarButtonItemAction(_ sender: Any) {
        
        self.performSegue(withIdentifier: SegueIdentifier.parameterShowToParameterEditSegue, sender: self)
    }
    
    @objc func deleteParameterBarButtonItemAction(_ sender: Any) {
        
        let deleteHandler: (UIAlertAction) -> Void = {
            alertAction in
            
            self.parameterController.delete(parameter: self.parameter!)
            self.performSegue(withIdentifier: SegueIdentifier.parameterShowToParameterHomeUnwindSegue, sender: self)
        }
        
        let cancelHandler: (UIAlertAction) -> Void = { alertAction in
            print("The \"Cancel\" alert occured.")
        }
        
        let alert = AlertController().deleteAlert(
            with: "Delete",
            message: "Do you want to delete \(parameter!.name!)",
            deleteHandler: deleteHandler,
            cancelHandler: cancelHandler)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        // Check if parameter injection is successful
        if parameter == nil {
            fatalError("Parameter is nil")
        }
        
        configureController()
        configureRightBarButtonItem()
        
        controller.configureAttributeData(parameter: parameter!)
        controller.configureTableView()
        controller.configureDataSource(parameter: parameter!)
        controller.apply()
        
    }
    
    func configureController() {
        controller = ParameterShowController(tableView: tableView, sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        controller.reloadData()
    }
    
    func configureRightBarButtonItem() {
        let editParameterBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(self.editParameterBarButtonItemAction(_:)))
        let deleteParameterBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(self.deleteParameterBarButtonItemAction(_:)))
        deleteParameterBarButtonItem.tintColor = UIColor.systemRed
        
        self.navigationItem.rightBarButtonItems = [editParameterBarButtonItem, deleteParameterBarButtonItem]
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.identifier {
        case SegueIdentifier.parameterShowToParameterEditSegue:
            
            guard let navigationController = segue.destination as? UINavigationController, let parameterEditTVC = navigationController.topViewController as? ParameterEditTableViewController else {
                fatalError("Wrong segue for ParameterEditTVC")
            }
            parameterEditTVC.context = context
            parameterEditTVC.parameter = parameter
            
            break
        default:
            break
        }
    }
    
    @IBAction func unwindToParameterShow(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        print("Unwind Segue from \(sourceViewController)")
        // Use data from the view controller which initiated the unwind segue
        print("unwind")
        context?.refresh(parameter!, mergeChanges: false)
        controller.apply()
        controller.configureAttributeData(parameter: parameter!)
        controller.reloadData()
    }
    
}
