//
//  EventEditTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 18/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class EventEditTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    var event: Event?
    
    lazy var eventController = EventController(context: context!)
    var controller: EventCreateController!
    let validator = EventFormValidator()
    
    let sections: [FormSectionType] = [.name, .date, .notes]
    
    let formItems: [FormSectionType: [CreateFormItem]] = [
        .name: [CreateFormItem(type: .name, placeholder: "Name (Required)")],
        .date: [CreateFormItem(type: .date, placeholder: "Date")],
        .notes: [CreateFormItem(type: .notes, placeholder: "Short Description (Optional)")]
    ]
    
    let cellReuseIdentifiers : [String: String] = [
        CellReuseIdentifier.eventCreateNameTVCell: NibName.formTextFieldTVCell,
        CellReuseIdentifier.eventCreateDateTVCell: NibName.formDatePickerViewTVCell,
        CellReuseIdentifier.eventCreateNotesTVCell: NibName.formTextViewTVCell
    ]
    
    @IBAction func cancelBarButtonItemAction(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: SegueIdentifier.eventEditToEventShowUnwindSegue, sender: self)
    }
    
    @IBOutlet weak var saveBarButtonItem: UIBarButtonItem!
    
    @IBAction func saveBarButtonItemAction(_ sender: UIBarButtonItem) {
        
        eventController.update(event: event!, with: controller.form)
        
        self.performSegue(withIdentifier: SegueIdentifier.eventEditToEventShowUnwindSegue, sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        // Check if event injection is successful
        if event == nil {
            fatalError("Event is nil")
        }
        
        configureController()
        
        controller.configureForm(with: event!)
        controller.configureTableView()
        controller.configureDataSource(viewController: self, aquariumHandler: nil)
        
        controller.apply()
    }
    
    func configureController() {
        controller = EventCreateController(tableView: tableView, form: EventForm(), sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
        
    }
}

// MARK: - Form Table View Cell Protocol

extension EventEditTableViewController: FormTableViewCellProtocol {
    
    func valueChanged(in cell: UITableViewCell, with value: Any) {
        
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        guard let formItem = controller.itemFor(indexPath: indexPath) else { return }
        var hasError: (Bool, [String])?
        
        switch formItem.type{
        case .name:
            controller.form.name = value as! String
            hasError = validator.validate(name: controller.form.name)
        case .date:
            controller.form.date = value as! Date
        case .notes:
            controller.form.notes = value as! String
        default:
            break
        }
        
        if let hasError = hasError {
            let errorableCell = cell as! FormErrorable
            errorableCell.showErrorUI(hasError.0)
            saveBarButtonItem.isEnabled = validator.validate(editForm: controller.form)
        }
    }
 
}

