//
//  ParameterEditTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 17/03/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class ParameterEditTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    var parameter: Parameter?
    
    lazy var parameterController = ParameterController(context: context!)
    var controller: ParameterCreateController!
    let validator = ParameterFormValidator()
    
    let sections: [FormSectionType] = [.name, .date, .attributes]
    
    let formItems: [FormSectionType: [CreateFormItem]] = [
        .name: [CreateFormItem(type: .name, placeholder: "Name (Required)")],
        .date: [CreateFormItem(type: .date, placeholder: "Date")]
    ]
    
    let cellReuseIdentifiers : [String: String] = [
        CellReuseIdentifier.parameterCreateNameTVCell: NibName.formTextFieldTVCell,
        CellReuseIdentifier.parameterCreateDateTVCell: NibName.formDatePickerViewTVCell,
        CellReuseIdentifier.parameterCreateAttributeTVCell: NibName.formSliderTVCell
    ]
    
    @IBOutlet weak var saveBarButtonItem: UIBarButtonItem!
    
    @IBAction func saveBarButtonItemAction(_ sender: UIBarButtonItem) {
        
        parameterController.update(parameter: parameter!, with: controller.form)
        
        self.performSegue(withIdentifier: SegueIdentifier.parameterEditToParameterShowUnwindSegue, sender: self)
    }
    
    @IBAction func cancelBarButtonItemAction(_ sender: UIBarButtonItem) {
        
        self.performSegue(withIdentifier: SegueIdentifier.parameterEditToParameterShowUnwindSegue, sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        // Check if parameter injection is successful
        if parameter == nil {
            fatalError("Parameter is nil")
        }
        
        configureController()
        
        controller.configureAttributes()
        controller.configureForm(with: parameter!)
        controller.configureTableView()

        let unitHandler: (((AttributeType, Int, IndexPath)) -> Void)? = {
                (tupple: (AttributeType, Int, IndexPath)) in
                    self.performSegue(withIdentifier: SegueIdentifier.parameterEditToParameterUnitSegue, sender: (tupple.0, tupple.1, tupple.2))
            }
        controller.configureDataSource(viewController: self, aquariumHandler: nil, unitHandler: unitHandler)
        
        controller.apply()
        
    }
    
    func configureController() {
        controller = ParameterCreateController(tableView: tableView, form: ParameterForm(), sections: sections, cellReuseIdentifiers: cellReuseIdentifiers, formItems: formItems)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.identifier {
        case SegueIdentifier.parameterEditToParameterUnitSegue:
            
            guard let navigationController = segue.destination as? UINavigationController, let parameterUnitTVC = navigationController.topViewController as? ParameterUnitTableViewController else {
                fatalError("Wrong segue for ParameterUnitTVC")
            }
            
            let data = sender as! (AttributeType, Int, IndexPath)
            parameterUnitTVC.attributeType = data.0
            parameterUnitTVC.selectedUnit = data.1
            parameterUnitTVC.indexPath = data.2
            parameterUnitTVC.unwindSegueIdentifier = SegueIdentifier.parameterUnitToParameterEditUnwindSegue
            
        default:
            break
        }
    }
    
    @IBAction func unwindToParameterEdit(_ unwindSegue: UIStoryboardSegue) {
        //        let sourceViewController = unwindSegue.source
        // Use data from the view controller which initiated the unwind segue
        
        switch unwindSegue.identifier {
        case SegueIdentifier.parameterUnitToParameterEditUnwindSegue:
            let parameterUnitTVC = unwindSegue.source as! ParameterUnitTableViewController
            let selectedUnit = parameterUnitTVC.selectedUnit!
            let indexPath = parameterUnitTVC.indexPath!
            
            controller.form.attributes[indexPath.row].unit = selectedUnit
            
            controller.reloadData()
        default:
            break
        }
    }
}

// MARK: - Form Table View Cell Protocol

extension ParameterEditTableViewController: FormTableViewCellProtocol {
    
    func valueChanged(in cell: UITableViewCell, with value: Any) {
        
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        guard let formItem = controller.itemFor(indexPath: indexPath) else { return }
        var hasError: (Bool, [String])?
        
        switch formItem.type{
        case .name:
            controller.form.name = value as! String
            hasError = validator.validate(name: controller.form.name)
        case .date:
            controller.form.date = value as! Date
        case .aquarium:
            break
        case .attributes:
            let cellValue = value as! FormSliderTableViewCell.CellValue
            controller.form.attributes[indexPath.row].active = cellValue.isActive
            controller.form.attributes[indexPath.row].value = cellValue.value
            controller.form.attributes[indexPath.row].unit = cellValue.unit
            hasError = (false, [])
        default: break
        }
        
        if let hasError = hasError {
            let errorableCell = cell as! FormErrorable
            errorableCell.showErrorUI(hasError.0)
            saveBarButtonItem.isEnabled = validator.validate(editForm: controller.form)
        }
        
    }
}
