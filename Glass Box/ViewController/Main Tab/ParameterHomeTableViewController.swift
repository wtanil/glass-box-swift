//
//  ParameterHomeTableViewController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 21/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class ParameterHomeTableViewController: UITableViewController {
    
    var context: NSManagedObjectContext?
    var aquarium: Aquarium?
    
    var dataController: ParameterHomeController!
    
    let cellReuseIdentifiers : [String: String] = [
        CellReuseIdentifier.parameterHomeTVCell: NibName.parameterHomeTVCell
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if context injection from AppDelegate or SceneDelegate is successful
        if context == nil {
            fatalError("Core Data Context is nil")
        }
        
        // Check if aquarium injection is successful
        if aquarium == nil {
            fatalError("Aquarium is nil")
        }
        
        configureController()
        
        dataController.configureTableView()
        dataController.configureDataSource()
        dataController.fetch()
    }
    
    func configureController() {
        
        let fetchRequest: NSFetchRequest<Parameter> = Parameter.fetchRequest()
        let sortByDate = NSSortDescriptor(key: "date", ascending: false)
        fetchRequest.sortDescriptors = [sortByDate]
        fetchRequest.predicate = NSPredicate(format: "aquarium = %@", aquarium!)
        let fetchedResultsController = NSFetchedResultsController<Parameter>(
            fetchRequest: fetchRequest,
            managedObjectContext: context!,
            sectionNameKeyPath: #keyPath(Parameter.sectionName),
            cacheName: nil)
        fetchedResultsController.delegate = self
        
        dataController = ParameterHomeController(tableView: tableView, fetchedResultsController: fetchedResultsController, cellReuseIdentifiers: cellReuseIdentifiers)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataController.reloadData()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        switch segue.identifier {
        case SegueIdentifier.parameterHomeToParameterShowSegue:
            guard let parameterShowTVC = segue.destination as? ParameterShowTableViewController else {
                fatalError("Wrong segue for ParameterShowTVC")
            }
            
            parameterShowTVC.context = context
            
            if let parameter = dataController.object(at: sender as! IndexPath) {
                parameterShowTVC.parameter = parameter
            }
            
        default:
            break
        }
    }
    
    @IBAction func unwindToParameterHome(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        print("Unwind Segue from \(sourceViewController)")
        // Use data from the view controller which initiated the unwind segue
        
    }
}

// MARK: - Table view data source
extension ParameterHomeTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dataController.didSelectRow(in: self, at: indexPath)
    }
}

extension ParameterHomeTableViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChangeContentWith snapshot: NSDiffableDataSourceSnapshotReference) {
        
        print("ParameterHomeTVC controller didChangeContentWith")
        dataController.apply(snapshot: snapshot as NSDiffableDataSourceSnapshot<String, NSManagedObjectID>, animatingDifferences: true)
        
    }
    
}
