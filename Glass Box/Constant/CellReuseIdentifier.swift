//
//  CellReuseIdentifier.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 04/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import Foundation

public struct CellReuseIdentifier {
    
    static let homeCreateTVCell: String = "HomeCreateTVCell"
    
    static let homeMainTVCell: String = "HomeMainTVCell"
    static let homeSettingTVCell: String = "HomeSettingTVCell"
    
    static let aquariumCreateNameTVCell: String = "FormNameCell"
    static let aquariumCreateDateTVCell: String = "FormDateCell"
    static let aquariumCreateTypeTVCell: String = "FormTypeCell"
    static let aquariumCreateSynopsisTVCell: String = "FormSynopsisCell"
    
    static let aquariumShowNameTVCell: String = "ShowTitleCell"
    static let aquariumShowSynopsisTVCell: String = "ShowSynopsisCell"
    static let aquariumShowDateTVCell: String = "ShowDateCell"
    static let aquariumShowTypeTVCell: String = "ShowTypeCell"
    static let aquariumShowEventTVCell: String = "ShowEventCell"
    static let aquariumShowParameterTVCell:String = "ShowAquariumParameterCell"
    static let aquariumShowLineChartTVCell = "ShowLineChartCell"
    
    static let eventHomeTVCell: String = "EventHomeCell"
    
    static let eventCreateNameTVCell: String = "FormNameCell"
    static let eventCreateDateTVCell: String = "FormDateCell"
    static let eventCreateNotesTVCell: String = "FormSynopsisCell"
    static let eventCreateAquariumTVCell: String = "FormAquariumCell"
    
    static let aquariumSelectTVCell = "AquariumSelectCell"
    
    static let eventShowNameTVCell: String = "ShowTitleCell"
    static let eventShowDateTVCell: String = "ShowDateCell"
    static let eventShowNotesTVCell: String = "ShowNotesCell"
    
    static let parameterHomeTVCell: String = "ParameterHomeCell"
    
    static let parameterCreateNameTVCell: String = "FormNameCell"
    static let parameterCreateDateTVCell: String = "FormDateCell"
    static let parameterCreateAquariumTVCell: String = "FormAquariumCell"
    static let parameterCreateAttributeTVCell: String = "FormAttributeCell"
    
    static let parameterShowNameTVCell: String = "ShowTitleCell"
    static let parameterShowDateTVCell: String = "ShowDateCell"
    static let parameterShowParameterTVCell:String = "ShowParameterCell"
    
    
    
    static let unitEditTVCell: String = "SettingUnitCell"
    
    static let unitSelectTVCell: String = "UnitSelectCell"
    
    static let parameterUnitTVCell: String = "ParameterUnitCell"
}
