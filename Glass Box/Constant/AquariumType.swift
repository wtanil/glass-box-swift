//
//  AquariumType.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 29/11/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import Foundation

public enum AquariumType: String, CaseIterable {
    case freshwater = "Fresh Water"
    case saltwater = "Salt Water"
    case brackishwater = "Brackish Water"
    case other = "Other"
}
