//
//  NibName.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 11/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import Foundation

public struct NibName {

    // FORM
    static let formTextFieldTVCell: String = "FormTextFieldTableViewCell"
    static let formTextViewTVCell: String = "FormTextViewTableViewCell"
    static let formDatePickerViewTVCell: String = "FormDatePickerViewTableViewCell"
    static let formPickerViewTVCell: String = "FormPickerViewTableViewCell"
    static let formMultipleChoicesTVCell: String = "FormMultipleChoicesTableViewCell"
    static let formMultipleSelectionTVCell = "FormMultipleSelectionTableViewCell"
    static let formSliderTVCell = "FormSliderTableViewCell"
    
    static let homeMainTVCell: String = "HomeMainTableViewCell"

    static let showTitleTVCell: String = "ShowTitleTableViewCell"
    static let showSynopsisTVCell: String = "ShowSynopsisTableViewCell"
    static let showDateTVCell: String = "ShowDateTableViewCell"
    static let showTypeTVCell: String = "ShowTypeTableViewCell"
    static let showEventTVCell: String = "ShowEventTableViewCell"
    static let showAquariumParameterTVCell = "ShowAquariumParameterTableViewCell"
    static let showParameterTVCell = "ShowParameterTableViewCell"
    static let showLineChartTVCell = "ShowLineChartTableViewCell"
    
    static let eventHomeTVCell: String = "EventHomeTableViewCell"
    
    static let parameterHomeTVCell: String = "ParameterHomeTableViewCell"
    
    static let settingUnitTVC: String = "SettingUnitTableViewCell"
    
    
    
    
}
