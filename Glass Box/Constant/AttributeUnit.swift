//
//  AttributeUnit.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 03/03/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation



public enum UnitType: String, CaseIterable {
    case none = "" // 0
    case ppm = "ppm" // 1
    case celcius = "°C" // 2
    case kelvin = "K" // 3
    case fahrenheit = "°F" // 4
    
}

public struct AttributeUnitType {
    static let value: [AttributeType: [Int]] = [
        AttributeType.ammonia : [0, 1],
        AttributeType.carbonDioxide : [0, 1],
        AttributeType.carbonHardness : [0, 1],
        AttributeType.chloramine : [0, 1],
        AttributeType.chlorine : [0, 1],
        AttributeType.generalHardness : [0, 1],
        AttributeType.nitrate : [0, 1],
        AttributeType.nitrite : [0, 1],
        AttributeType.pH : [0],
        AttributeType.phosphate : [0, 1],
        AttributeType.temperature : [2, 3, 4],
        AttributeType.salinity : [0]
    ]
}
