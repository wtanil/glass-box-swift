//
//  SegueIdentifier.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 04/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import Foundation

public struct SegueIdentifier {
    
    static let homeCreateToAquariumCreateSegue = "homeCreateToAquariumCreateSegue"
    static let homeCreateToEventCreateSegue = "homeCreateToEventCreateSegue"
    static let homeCreateToParameterCreateSegue = "homeCreateToParameterCreateSegue"
    
    static let aquariumCreateToHomeCreateUnwindSegue = "aquariumCreateToHomeCreateUnwindSegue"
    
    static let homeMainToAquariumShowSegue = "homeMainToAquariumShowSegue"
    
    static let aquariumShowToHomeMainUnwindSegue = "aquariumShowToHomeMainUnwindSegue"
    
    static let aquariumShowToAquariumEditSegue = "aquariumShowToAquariumEditSegue"
    
    static let aquariumEditToAquariumShowUnwindSegue = "aquariumEditToAquariumShowUnwindSegue"
    
    static let aquariumShowToEventHomeSegue = "aquariumShowToEventHomeSegue"
    
    static let eventCreateToAquariumSelectSegue = "eventCreateToAquariumSelectSegue"
    
    static let aquariumSelectToEventCreateUnwindSegue = "aquariumSelectToEventCreateUnwindSegue"
    
    static let eventCreateToHomeCreateUnwindSegue = "eventCreateToHomeCreateUnwindSegue"
    
    static let eventHomeToEventShowSegue = "eventHomeToEventShowSegue"
    
    static let eventShowToEventHomeUnwindSegue = "eventShowToEventHomeUnwindSegue"
    
    static let eventShowToEventEditSegue = "eventShowToEventEditSegue"
    
    static let eventEditToEventShowUnwindSegue = "eventEditToEventShowUnwindSegue"
    
    static let aquariumShowToParameterHomeSegue = "aquariumShowToParameterHomeSegue"
    
    static let parameterCreateToHomeCreateUnwindSegue = "parameterCreateToHomeCreateUnwindSegue"
    
    static let parameterCreateToAquariumSelectSegue = "parameterCreateToAquariumSelectSegue"
    
    static let aquariumSelectToParameterCreateUnwindSegue = "aquariumSelectToParameterCreateUnwindSegue"
    
    static let parameterHomeToParameterShowSegue = "parameterHomeToParameterShowSegue"
    
    static let parameterShowToParameterHomeUnwindSegue = "parameterShowToParameterHomeUnwindSegue"
    
    static let parameterShowToParameterEditSegue = "parameterShowToParameterEditSegue"
    
    static let parameterEditToParameterShowUnwindSegue = "parameterEditToParameterShowUnwindSegue"
    
    static let homeSettingToUnitEditSegue = "homeSettingToUnitEditSegue"
    
    static let unitEditToUnitSelectSegue = "unitEditToUnitSelectSegue"
    
    static let parameterCreateToParameterUnitSegue = "parameterCreateToParameterUnitSegue"
    
    static let parameterUnitToParameterCreateUnwindSegue = "parameterUnitToParameterCreateUnwindSegue"
    
    static let parameterEditToParameterUnitSegue = "parameterEditToParameterUnitSegue"
    
    static let parameterUnitToParameterEditUnwindSegue = "parameterUnitToParameterEditUnwindSegue"
}
