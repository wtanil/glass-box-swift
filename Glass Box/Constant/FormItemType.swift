//
//  FormItemType.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 30/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

enum FormItemType: CaseIterable {
    case name, date, attribute, notes, type, synopsis, updateDate, event, parameter, aquarium, attributes, lineChart
}

//enum CreateFormItemType: CaseIterable {
//    case name, date, type, synopsis, notes, aquarium, attributes
//}

//enum ShowFormItemType: CaseIterable {
//    case name, date, attribute, notes, type, synopsis, updateDate, event, parameter
//}
