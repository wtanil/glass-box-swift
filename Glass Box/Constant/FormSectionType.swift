//
//  FormSectionType.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 30/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

enum FormSectionType: CaseIterable {
    case main
    case name, type, date, synopsis, aquarium, notes, attributes, lineChart
    case updateDate, event, parameter
}

//enum UnitEditFormSectionType: CaseIterable {
//    case main
//}

//enum ParameterUnitFormSectionType: CaseIterable {
//    case main
//}

//enum CreateFormSectionType: CaseIterable {
//    case name, type, date, synopsis, aquarium, notes, attributes
//}
