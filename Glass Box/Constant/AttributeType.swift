//
//  AttributeType.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 26/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

public enum AttributeType: String, CaseIterable {
    
    case ammonia = "Ammonia" // 0
    case carbonDioxide = "Carbon Dioxide" // 1
    case carbonHardness = "Carbon Hardness" // 2
    case chloramine = "Chloramine" // 3
    case chlorine = "Chlorine" // 4
    case generalHardness = "General Hardness" // 5
    case nitrate = "Nitrate" // 6
    case nitrite = "Nitrite" // 7
    case pH = "pH" // 8
    case phosphate = "Phosphate" // 9
    case temperature = "Temperature" // 10
    case salinity = "Salinity" // 11
    
}

// case freshwater = "Fresh Water"
//case saltwater = "Salt Water"
//case brackishwater = "Brackish Water"
//case other = "Other"

public struct AquariumAttributeType {
    static let value: [AquariumType: [Int]] = [
        AquariumType.freshwater: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        AquariumType.saltwater: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
        AquariumType.brackishwater: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
        AquariumType.other: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    ]
}
