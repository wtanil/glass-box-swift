//
//  AxisDateValueFormatter.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 05/06/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation
import Charts

class AxisDateValueFormatter: IAxisValueFormatter {
    
    private let dateFormatter = DateFormatter()
    
    init(dateFormat: String) {
        
        dateFormatter.dateFormat = dateFormat
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return dateFormatter.string(from: Date(timeIntervalSince1970: value))
    }
    
}




