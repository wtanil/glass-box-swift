//
//  AxisIntValueFormatter.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 10/06/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation
import Charts

class AxisIntValueFormatter: IAxisValueFormatter {
    
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let intValue = Int(value)
        let string = String(intValue)
        return string
    }
    
}
