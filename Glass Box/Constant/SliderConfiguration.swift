//
//  SliderConfiguration.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 28/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

public struct SliderConfiguration {
    
    static let value: [AttributeType: (Float, Float)] = [
        AttributeType.ammonia : (0.0, 50.0),
        AttributeType.carbonDioxide : (0.0, 50.0),
        AttributeType.carbonHardness : (0.0, 50.0),
        AttributeType.chloramine : (0.0, 50.0),
        AttributeType.chlorine : (0.0, 50.0),
        AttributeType.generalHardness : (0.0, 50.0),
        AttributeType.nitrate : (0.0, 50.0),
        AttributeType.nitrite : (0.0, 50.0),
        AttributeType.pH : (0.0, 14.0),
        AttributeType.phosphate : (0.0, 50.0),
        AttributeType.temperature : (0.0, 50.0),
        AttributeType.salinity : (0.0, 50.0)
        
    ]
}
