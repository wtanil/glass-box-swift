//
//  Constant.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 20/03/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

public struct Constant {
    
    static let unitKey = "units."
    static let unitCheckKey = "units.check"
    
}
