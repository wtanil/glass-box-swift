//
//  AquariumController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 03/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import UIKit

import CoreData

class AquariumController {
    
    private var context: NSManagedObjectContext
    
    public init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    private func save() {
        if context.hasChanges {
            do {
                try context.save()
            } catch let error as NSError {
                print("Core Data on save error: \(error.localizedDescription)")
                fatalError()
            }
        }
    }
    
    public func create(with name: String, type: Int, date: Date, synopsis: String) {
        
        let aquarium = Aquarium(context: context)
        aquarium.name = name
        aquarium.type = Int16(type)
        aquarium.date = date
        aquarium.synopsis = synopsis
        aquarium.createDate = Date()
        aquarium.updateDate = Date()
        
        save()
    }
    
    public func create(with form: AquariumForm) {
        
        let aquarium = Aquarium(context: context)
        aquarium.name = form.name
        aquarium.type = Int16(form.type)
        aquarium.date = form.date
        aquarium.synopsis = form.synopsis
        aquarium.createDate = Date()
        aquarium.updateDate = Date()
        
        save()
    }
    
    public func delete(aquarium: Aquarium) {
        
        context.delete(aquarium)
        save()
    }
    
    public func update(aquarium: Aquarium, with form: AquariumForm) {
        aquarium.name = form.name
        aquarium.date = form.date
        aquarium.synopsis = form.synopsis
        aquarium.updateDate = Date()
        
        save()
        
    }
    
    public func latestParameterAttributesFor(aquarium: Aquarium) -> [Attribute] {
        
        var attributeArray = [Attribute]()
        
        if aquarium.attributes != nil {
            
            let attributeTypeCount = AttributeType.allCases.count
            for index in 0...attributeTypeCount {
                
                if let attribute = fetchLatestAttribute(with: Int16(index), for: aquarium) {
                    attributeArray.append(attribute)
                }
            }
        }
        return attributeArray
    }
    
    private func fetchLatestAttribute(with type: Int16, for aquarium: Aquarium) -> Attribute? {
        
        let fetchRequest: NSFetchRequest<Attribute> = Attribute.fetchRequest()
        let aquariumPredicate = NSPredicate(format: "aquarium = %@", aquarium)
        let typePredicate = NSPredicate(format: "type = %i", type)
        let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [aquariumPredicate, typePredicate])
        fetchRequest.predicate = compoundPredicate
        let sortByDate = NSSortDescriptor(key: "date", ascending: false)
        fetchRequest.sortDescriptors = [sortByDate]
        fetchRequest.fetchLimit = 1
        
        do {
            let attributes = try context.fetch(fetchRequest)
            if attributes.isEmpty {
                return nil
            } else {
                return attributes[0]
            }
        } catch let error as NSError {
            fatalError("Core Data on fetch error: \(error.localizedDescription) \(error.userInfo)")
        }
        
    }
    
    
    
    
    
}
