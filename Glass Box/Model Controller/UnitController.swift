//
//  UnitController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 19/03/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class UnitController {
    
    private let defaults: UserDefaults
    
    init() {
        defaults = UserDefaults.standard
    }
    
    public func set(key: String, with value: Int) {
        defaults.set(value, forKey: key)
    }
    
    public func get(key: String ) -> Int {
        return defaults.integer(forKey: key)
    }
    
    public func keyFor(attributeName: String) -> String {
        return "\(Constant.unitKey)\(attributeName)"
    }
    
    func isDefaultSet() -> Bool {
        let key = "\(Constant.unitCheckKey)"
        if get(key: key) == 1 {
            return true
        }
        return false
    }
    
    func getAlert(checkAction: ((UIAlertAction) -> Void)?) -> UIAlertController {
        let key = "\(Constant.unitCheckKey)"
        UnitController().set(key: key, with: 1)
        
        let cancelHandler: (UIAlertAction) -> Void = { alertAction in
            print("The \"Cancel\" alert occured.")
        }
        
        let alert = AlertController().checkAlert(
        with: "Default parameter unit",
        message: "Do you want to set default unit for parameters?",
        checkHandler: checkAction,
        cancelHandler: cancelHandler)
        
        return alert
    }
}
