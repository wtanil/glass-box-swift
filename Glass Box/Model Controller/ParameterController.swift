//
//  ParameterController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 26/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class ParameterController {
    
    private var context: NSManagedObjectContext
    
    public init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    private func save() {
        if context.hasChanges {
            do {
                try context.save()
            } catch let error as NSError {
                print("Core Data on save error: \(error.localizedDescription)")
                fatalError()
            }
        }
    }
    
    public func create(with form: ParameterForm) {
        let syncDate = Date()
        let type: [AquariumType] = AquariumType.allCases
        
        for aquarium in form.aquarium {
            let parameter = Parameter(context: context)
            
            parameter.aquarium = aquarium
            
            parameter.name = form.name
            parameter.date = form.date
            parameter.createDate = syncDate
            parameter.updateDate = syncDate
            
            let parameterAttribute = ParameterAttribute(context: context)
            parameter.parameterAttribute = parameterAttribute
            
            let typeIndex = Int(aquarium.type)
            let aquariumType = type[typeIndex]
            guard let attributeTypeIndex = AquariumAttributeType.value[aquariumType] else {
                fatalError("Index configuration doesn't exist")
            }
            
            
            for index in attributeTypeIndex {
                if form.attributes[index].active {
                    let tempAttribute = Attribute(context: context)
                    tempAttribute.value = form.attributes[index].value
                    tempAttribute.type = form.attributes[index].type
                    tempAttribute.date = form.date
                    tempAttribute.unit = Int16(form.attributes[index].unit)
                    
                    tempAttribute.parameterAttribute = parameterAttribute
                    tempAttribute.aquarium = aquarium
                    
                }
            }
            
        }
        
        save()
    }
    
    public func delete(parameter: Parameter) {
        
        context.delete(parameter)
        save()
    }
    
    public func update(parameter: Parameter, with form: ParameterForm) {
        
        let syncDate = Date()
        let type: [AquariumType] = AquariumType.allCases
        
        parameter.name = form.name
        parameter.date = form.date
        parameter.updateDate = syncDate
        
        
        let typeIndex = Int(parameter.aquarium!.type)
        let aquariumType = type[typeIndex]
        guard let attributeTypeIndex = AquariumAttributeType.value[aquariumType] else {
            fatalError("Index configuration doesn't exist")
        }
        
        /*
         if let currentAttributes = parameter?.parameterAttribute?.attributes {
         
         let convertedAttributes = currentAttributes as! Set<Attribute>
         
         for attribute in convertedAttributes {
         if let index = parameterForm.attributes.firstIndex(where: { $0.type == attribute.type }) {
         parameterForm.attributes[index].active = true
         parameterForm.attributes[index].value = attribute.value
         parameterForm.attributes[index].unit = Int(attribute.unit)
         } else {
         print("NOT FOUND")
         }
         }
         
         print("configuredata parametere form \(parameterForm)")
         }
         */
        
        let currentAttributes = parameter.parameterAttribute?.attributes as! Set<Attribute>?
        
        for index in attributeTypeIndex {
            let convertedIndex = Int16(index)
            let attributeForm = form.attributes[index]
            // Check if attribute is active
            if attributeForm.active {
                // Check if attribute exists, update attribute if exists, else create a new attribute
                if let currentAttribute = currentAttributes?.first(where: { $0.type == convertedIndex}){
                    
                    currentAttribute.value = attributeForm.value
                    currentAttribute.type = attributeForm.type
                    currentAttribute.date = form.date
                    currentAttribute.unit = Int16(attributeForm.unit)
                    
                } else {
                    let tempAttribute = Attribute(context: context)
                    tempAttribute.value = attributeForm.value
                    tempAttribute.type = attributeForm.type
                    tempAttribute.date = form.date
                    tempAttribute.unit = Int16(attributeForm.unit)
                    
                    tempAttribute.parameterAttribute = parameter.parameterAttribute
                    tempAttribute.aquarium = parameter.aquarium

                }
                
            } else {
                // Check if attribute exists, delete attribute if exists
                if let currentAttribute = currentAttributes?.first(where: { $0.type == convertedIndex}){
                    delete(attribute: currentAttribute)
                }
            }
        }
        save()
    }
    
    private func delete(attribute: Attribute) {
        context.delete(attribute)
        save()
    }
    
    
    
    public func get(for aquarium: Aquarium, from: Date, to: Date) -> [Parameter] {
        
        let fetchRequest: NSFetchRequest<Parameter> = Parameter.fetchRequest()
        
        let aquariumPredicate = NSPredicate(format: "aquarium = %@", aquarium)
        let dateRangePredicate = NSPredicate(format: "date >= %@ AND date <= %@", argumentArray: [from, to])
        let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [aquariumPredicate, dateRangePredicate])
        
        fetchRequest.predicate = compoundPredicate
        
        let sortByDate = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [sortByDate]
        
        do {
            let parameters = try self.context.fetch(fetchRequest)
            
            return parameters
            
        } catch let error as NSError {
            print("Core Data on save error: \(error.localizedDescription) \(error.userInfo)")
            fatalError()
        }
        
    }
    
    
}
