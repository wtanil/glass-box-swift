//
//  EventController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 26/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation
import CoreData

class EventController {
    
    private var context: NSManagedObjectContext
    
    public init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    private func save() {
        if context.hasChanges {
            do {
                try context.save()
            } catch let error as NSError {
                print("Core Data on save error: \(error.localizedDescription)")
                fatalError()
            }
        }
    }
    
    public func create(with form: EventForm) {
        
        let syncDate = Date()
        
        for aquarium in form.aquarium {
            let event = Event(context: context)
            
            event.aquarium = aquarium
            
            event.name = form.name
            event.date = form.date
            event.notes = form.notes
            event.createDate = syncDate
            event.updateDate = syncDate
            
        }
        
        save()
    }
    
    public func delete(event: Event) {
        
        context.delete(event)
        save()
    }
    
    public func update(event: Event, with form: EventForm) {
        
        event.name = form.name
        event.date = form.date
        event.notes = form.notes
        event.updateDate = Date()
        
        save()
        
    }
    
    public func latestEventFor(aquarium: Aquarium) -> Event? {
        let fetchRequest: NSFetchRequest<Event> = Event.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "aquarium = %@", aquarium)
        let sortByDate = NSSortDescriptor(key: "date", ascending: false)
        fetchRequest.sortDescriptors = [sortByDate]
        fetchRequest.fetchLimit = 1
        
        do {
            let events = try self.context.fetch(fetchRequest)
            if events.isEmpty {
                return nil
            } else {
                return events[0]
            }
            
        } catch let error as NSError {
            print("Core Data on save error: \(error.localizedDescription) \(error.userInfo)")
            fatalError()
        }
    }
    
    
    public func get(for aquarium: Aquarium, from: Date, to: Date) -> [Event] {
        
        let fetchRequest: NSFetchRequest<Event> = Event.fetchRequest()
        
        let aquariumPredicate = NSPredicate(format: "aquarium = %@", aquarium)
        let dateRangePredicate = NSPredicate(format: "date >= %@ AND date <= %@", argumentArray: [from, to])
        let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [aquariumPredicate, dateRangePredicate])
        
        fetchRequest.predicate = compoundPredicate
        
        let sortByDate = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [sortByDate]
        
//        fetchRequest.propertiesToGroupBy
//        fetchRequest.resultType = .dictionaryResultType
//
//        let countExpression = NSExpression(format: "count:(name)")
//        let countExpressionDescription = NSExpressionDescription()
//        countExpressionDescription.expression = countExpression
//        countExpressionDescription.name = "count"
//        countExpressionDescription.expressionResultType = .integer16AttributeType
//
//        fetchRequest.propertiesToGroupBy = ["sectionName"]
//        fetchRequest.propertiesToFetch = ["date", countExpressionDescription]
//
        
        do {
            let events = try self.context.fetch(fetchRequest)
            
            return events
            
        } catch let error as NSError {
            print("Core Data on save error: \(error.localizedDescription) \(error.userInfo)")
            fatalError()
        }
        
    }
    
}
