//
//  EventFormValidator.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 13/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

class EventFormValidator: Validator {
    
    func validate(name: String) -> (Bool, [String]) {
        
        var messages = [String]()
        var hasError = false
        
        if !validateStringNotEmpty(value: name) {
            messages.append("Name is required.")
            hasError = setErrorBool(from: hasError, to: true)
        }
        
        //        if !validateStringHasMinimumLength(value: name, minimumLength: 3) {
        //            messages.append("Minimum character is 3.")
        //            isError = setErrorBool(from: isError, to: true)
        //        }
        
        return (hasError, messages)
    }
    
//    func validate(date: Date) -> (Bool, [String]) {
//
//        var messages = [String]()
//        var hasError = false
//
//        if !validateDateBeforeNow(value: date) {
//            messages.append("Date is after now.")
//            hasError = setErrorBool(from: hasError, to: true)
//        }
//
//        return (hasError, messages)
//    }
    
    func validate(aquariums: [Aquarium]) -> (Bool, [String]) {
        
        var messages = [String]()
        var hasError = false
        
        if !validateArrayNotEmpty(value: aquariums) {
            messages.append("At least one aquarium is required")
            hasError = setErrorBool(from: hasError, to: true)
        }
        
        return (hasError, messages)
        
    }
    
    func validate(createForm form: EventForm) -> Bool {
        let (nameHasError, _) = validate(name: form.name)
        
        if nameHasError {
            return false
        }
//        
//        let (dateHasError, _) = validate(date: form.date)
//        
//        if dateHasError {
//            return false
//        }
        
        let (aquariumsHasError, _) = validate(aquariums: form.aquarium)
        
        if aquariumsHasError {
            return false
        }
        
        return true
    }
    
    func validate(editForm form: EventForm) -> Bool {
        let (nameHasError, _) = validate(name: form.name)
        
        if nameHasError {
            return false
        }
        
        return true
    }
    
}
