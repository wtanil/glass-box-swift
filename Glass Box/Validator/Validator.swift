//
//  Validator.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 20/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import Foundation

public class Validator {
    
    public func setErrorBool(from oldValue: Bool, to newValue: Bool) -> Bool {
        if oldValue {
            return true
        }
        return newValue
    }
    
    public func validateStringNotEmpty(value: String!) -> Bool {
        
        if value.isEmpty {
            return false
        }
        
        return true
        
    }
    
    public func validateDateBeforeNow(value: Date!)-> Bool {
        
        if value > Date() {
            return false
        }
        
        return true
    }
    
    public func validateNameUnique() {
        
    }
    
    public func validateStringHasMinimumLength(value: String, minimumLength: Int) -> Bool {
        
        if value.count < minimumLength {
            return false
        }
        
        return true
    }
    
    public func validateArrayNotEmpty(value: [Any]) -> Bool {
        
        if value.isEmpty {
            return false
        }
        
        return true
    }
    
    
}
