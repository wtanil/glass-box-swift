//
//  ParameterFormValidator.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 14/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

class ParameterFormValidator: Validator {
    
    func validate(name: String) -> (Bool, [String]) {
        
        var messages = [String]()
        var hasError = false
        
        if !validateStringNotEmpty(value: name) {
            messages.append("Name is required.")
            hasError = setErrorBool(from: hasError, to: true)
        }
        
        return (hasError, messages)
    }
    
    func validate(aquariums: [Aquarium]) -> (Bool, [String]) {
        
        var messages = [String]()
        var hasError = false
        
        if !validateArrayNotEmpty(value: aquariums) {
            messages.append("At least one aquarium is required")
            hasError = setErrorBool(from: hasError, to: true)
        }
        
        return (hasError, messages)
        
    }
    
    func validate(createForm form: ParameterForm) -> Bool {
        let (nameHasError, _) = validate(name: form.name)
        
        if nameHasError {
            return false
        }
        
        let (aquariumsHasError, _) = validate(aquariums: form.aquarium)
        
        if aquariumsHasError {
            return false
        }
        
        return true
    }
    
    func validate(editForm form: ParameterForm) -> Bool {
        let (nameHasError, _) = validate(name: form.name)
        
        if nameHasError {
            return false
        }
        
        return true
    }
    
}
