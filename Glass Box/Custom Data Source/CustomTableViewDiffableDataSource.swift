//
//  CustomTableViewDiffableDataSource.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 17/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class CustomTableViewDiffableDataSource: UITableViewDiffableDataSource<String, NSManagedObjectID> {
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        return snapshot().sectionIdentifiers[section]
    }

}
