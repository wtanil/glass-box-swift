//
//  EventForm.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 26/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

struct EventForm {
    var name: String = ""
    var date: Date = Date()
    var notes: String = ""
    var aquarium: [Aquarium] = []
}
