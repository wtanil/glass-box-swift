//
//  CreateFormItem.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 22/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

struct CreateFormItem: Hashable {
    
    let type: FormItemType
    var placeholder: String
    
    private let identifier: UUID
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.identifier)
    }
    
    init(type: FormItemType, placeholder: String) {
        self.type = type
        self.placeholder = placeholder
        self.identifier = UUID()
    }
}
