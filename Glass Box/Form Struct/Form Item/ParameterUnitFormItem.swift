//
//  ParameterUnitFormItem.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 28/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

struct ParameterUnitFormItem: Hashable {
    let title: String
    let value: Int
    private let identifier: UUID
    
    init(title: String, value: Int) {
        self.title = title
        self.value = value
        self.identifier = UUID()
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.identifier)
    }
}
