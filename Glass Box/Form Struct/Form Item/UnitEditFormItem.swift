//
//  UnitEditFormItem.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 29/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

struct UnitEditFormItem: Hashable {
    let title: String
    let value: Int
    let type: AttributeType
    private let identifier: UUID
    
    init(type: AttributeType, title: String, value: Int) {
        self.type = type
        self.title = title
        self.value = value
        self.identifier = UUID()
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.identifier)
    }
}
