//
//  HomeFormItem.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 27/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

struct HomeFormItem: Hashable {
    
    let title: String
    private let identifier: UUID
    
    init(title: String) {
        self.title = title
        self.identifier = UUID()
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.identifier)
    }
}
