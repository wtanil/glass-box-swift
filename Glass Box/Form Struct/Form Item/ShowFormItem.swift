//
//  ShowFormItem.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 30/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

struct ShowFormItem: Hashable {
    
    let type: FormItemType
    
    private let identifier: UUID
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.identifier)
    }
    
    init(type: FormItemType) {
        self.type = type
        self.identifier = UUID()
    }
    
}
