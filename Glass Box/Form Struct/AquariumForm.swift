//
//  AquariumForm.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 07/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

struct AquariumForm {
    var name: String = ""
    var type: Int = 0
    var date: Date = Date()
    var synopsis: String = ""
}
