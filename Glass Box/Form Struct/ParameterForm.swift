//
//  ParameterForm.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 25/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation

struct ParameterForm {
    var name: String = ""
    var date: Date = Date()
    var aquarium: [Aquarium] = []
    var attributes: [AttributeForm] = []
    
    struct AttributeForm {
//        var type: AttributeType
        var type: Int16
        var active: Bool = false
        var value: Float = 0.0
        var unit: Int = 0
        
    }
    
}
