//
//  SettingUnitTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 20/03/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class SettingUnitTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var unitLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    public func configureCell(with title: String, unit: String) {
        
        titleLabel.text = title
        unitLabel.text = unit
    }
    
}
