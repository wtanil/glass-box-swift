//
//  FormPickerViewTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 17/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import UIKit

class FormPickerViewTableViewCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    var pickerDataSource: [String] = [String]()
    
    weak var delegate: FormTableViewCellProtocol?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(with dataSource: [String], row: Int, labelText: String) {
        
        pickerDataSource = dataSource
        
        label.text = labelText
        pickerView.reloadAllComponents()
        pickerView.selectRow(row, inComponent: 0, animated: false)
        
    }
    
    // MARK: - UITextFieldDelegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        delegate?.valueChanged(in: self, with: row)
    }
    
}
