//
//  FormDatePickerViewTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 17/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import UIKit

class FormDatePickerViewTableViewCell: UITableViewCell, FormErrorable {
    
    @IBOutlet weak var label: UILabel!
    
    var labelText: String = ""
    var formattedLabelText: String {
        get {
            let dateString = DateFormatter.dateFormatterMediumShort.string(from: datePickerValue)
            if labelText.isEmpty {
                return dateString
            }
            return labelText + " " + dateString
        }
    }
    
    @IBOutlet weak var datePickerView: UIDatePicker!
    
    @IBAction func datePickerViewAction(_ sender: UIDatePicker, forEvent event: UIEvent) {
        
        datePickerValue = sender.date
        updateDateLabel()
        delegate?.valueChanged(in: self, with: datePickerValue)
        
    }
    
    private var datePickerValue = Date()
    
    weak var delegate: FormTableViewCellProtocol?
    var defaultBackgroundColor: UIColor?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        
        defaultBackgroundColor = self.contentView.backgroundColor
        //        datePickerView.maximumDate = Date()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    public func configureCell(with date: Date, labelText: String) {
        self.labelText = labelText
        updateDateLabel()
        datePickerValue = date
        datePickerView.setDate(datePickerValue, animated: true)
    }
    
    public func showErrorUI(_ value: Bool) {

        switch value {
        case true:
            self.contentView.backgroundColor = UIColor.systemRed
        default:
            self.contentView.backgroundColor = defaultBackgroundColor
        }
    }
    
    private func updateDateLabel() {
        label.text = formattedLabelText
    }
    
}
