//
//  FormMultipleChoicesTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 27/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class FormMultipleChoicesTableViewCell: UITableViewCell, FormErrorable {
    
    @IBAction func buttonAction(_ sender: UIButton, forEvent event: UIEvent) {
        action()
    }
    
    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var label: UILabel!
    
    var action: () -> Void = {}
    
    var placeholder: String = "Placeholder"
    var defaultTextColor: UIColor?
    
    var defaultBackgroundColor: UIColor?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        
        defaultTextColor = self.label.textColor
        defaultBackgroundColor = self.contentView.backgroundColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(aquariums: [String], placeholder: String, action: @escaping () -> Void) {
        
        self.action = action
        
        
        if aquariums.isEmpty {
            label.textColor = UIColor.placeholderText
            label.text = placeholder
        } else {
            label.textColor = defaultTextColor
//            label.text = aquariums.joined(separator: ", ")
            label.text = aquariums.joined(separator: ",\n")
        }
    }
    
    public func showErrorUI(_ value: Bool) {

        switch value {
        case true:
            self.contentView.backgroundColor = UIColor.systemRed
        default:
            self.contentView.backgroundColor = defaultBackgroundColor
        }
    }
    
    
}
