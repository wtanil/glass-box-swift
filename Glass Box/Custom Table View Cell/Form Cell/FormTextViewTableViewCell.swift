//
//  FormTextViewTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 16/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import UIKit

class FormTextViewTableViewCell: UITableViewCell, UITextViewDelegate {
    
    
    @IBOutlet weak var textView: UITextView!
    
    var placeholder: String = "Placeholder"
    var defaultTextColor: UIColor?
    
    weak var delegate: FormTableViewCellProtocol?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        textView.delegate = self
        
        defaultTextColor = self.textView.textColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(with description: String, placeholder: String) {
        textView.text = description
        self.placeholder = placeholder
        
        if description.isEmpty {
            showPlaceholder()
        }
    }
    
    // MARK: - UITextViewDelegate
    
    func textViewDidChange(_ textView: UITextView) {
        delegate?.valueChanged(in: self, with: textView.text!)
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.placeholderText {
            hidePlaceholder()
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            showPlaceholder()
        }
    }
    
    func showPlaceholder() {
        textView.text = placeholder
        textView.textColor = UIColor.placeholderText
    }
    
    func hidePlaceholder() {
        textView.text = ""
        textView.textColor = defaultTextColor
    }
    
}
