//
//  FormMultipleSelectionTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 10/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class FormMultipleSelectionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        selectionStyle = .none
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            accessoryType = .checkmark
        } else {
            accessoryType = .none
        }
        
    }
    
    public func configureCell(with name: String) {
        
        label.text = name
        
    }
    
}
