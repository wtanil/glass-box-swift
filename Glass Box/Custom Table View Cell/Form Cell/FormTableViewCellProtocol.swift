//
//  FormTableViewCellProtocol.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 10/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import UIKit

protocol FormTableViewCellProtocol: AnyObject {
    func valueChanged(in cell: UITableViewCell, with value: Any)
}

protocol FormErrorable {
    func showErrorUI(_ value: Bool)
}
