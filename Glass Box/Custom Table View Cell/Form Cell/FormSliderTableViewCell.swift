//
//  FormSliderTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 26/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class FormSliderTableViewCell: UITableViewCell, FormErrorable {
    
    struct CellValue {
        var isActive: Bool = false
        var value: Float = 0.0
        var unit: Int = 0
    }
    
    weak var delegate: FormTableViewCellProtocol?
    var cellValue = CellValue()
    
    // TITLE LABEL
    @IBOutlet weak var titleLabel: UILabel!
    // end of TITLE LABEL
    
    // ACTIVE BUTTON
    @IBOutlet weak var activeButton: UIButton!
    @IBAction func activeButtonAction(_ sender: UIButton) {
        
        cellValue.isActive.toggle()
        
        resetActiveButton()
        toggleComponents(status: cellValue.isActive)
        
        delegate?.valueChanged(in: self, with: cellValue)
    }
    
    private func resetActiveButton() {
        if cellValue.isActive {
            activeButton.setTitle("Disable", for: .normal)
            
        } else {
            activeButton.setTitle("Enable", for: .normal)
            
        }
    }
    
    private func toggleComponents(status: Bool) {
        slider.isEnabled = status
        textField.isEnabled = status
        unitButton.isEnabled = status
        
        if status {
            titleLabel?.textColor = UIColor.label
        } else {
            titleLabel?.textColor = UIColor.tertiaryLabel
        }
    }
    // end of ACTIVE BUTTON
    
    // TEXT FIELD
    @IBOutlet weak var textField: UITextField!
    @IBAction func textFieldAction(_ sender: UITextField, forEvent event: UIEvent) {
        
        let number = (textField.text! as NSString).floatValue
        if number > slider.maximumValue {
            slider.value = slider.maximumValue
        } else if number < slider.minimumValue {
            slider.value = slider.minimumValue
        } else {
            slider.value = number
        }
        
        cellValue.value = number

        delegate?.valueChanged(in: self, with: cellValue)
    }
    // end of TEXT FIELD
    
    
    // SLIDER BUTTON
    @IBOutlet weak var slider: UISlider!
    @IBAction func sliderAction(_ sender: UISlider) {
        
        let formattedValue = String(format: "%.1f", sender.value)
        textField.text = formattedValue
        // Stick to 1 decimal
        slider.value = (formattedValue as NSString).floatValue
        
        cellValue.value = slider.value
        
        delegate?.valueChanged(in: self, with: cellValue)
        
    }
    // end of SLIDER BUTTON
    
    
    // UNIT BUTTON
    @IBOutlet weak var unitButton: UIButton!
    @IBAction func unitButtonAction(_ sender: UIButton) {
        unitButtonAction()
    }
    
    var unitButtonAction: () -> Void = {}
    
    // end of UNIT BUTTON
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    public func configureCell(with title: String, isActive: Bool, value: Float,  sliderConfig: (Float, Float), unit: Int, unitTitle: String, action: @escaping () -> Void) {
        
        // Cell Value
        cellValue.isActive = isActive
        cellValue.value = value
        cellValue.unit = unit
        
        // Active Button
        resetActiveButton()
        toggleComponents(status: cellValue.isActive)
        
        // Title Label
        titleLabel.text = title
        
        // Text Field
        let formattedValue = String(format: "%.1f", cellValue.value)
        textField.text = formattedValue
        
        // Slider
        configureSlider(with: sliderConfig)
        slider.value = cellValue.value
        
        // Unit Button
        unitButton.setTitle(unitTitle, for: .normal)
        unitButtonAction = action
        //        unitButtonAction = action
        //        unitButton.setTitle("UNIT", for: .normal)
        
        
    }
    
    private func configureSlider(with minMax: (Float, Float)) {
        slider.minimumValue = minMax.0
        slider.maximumValue = minMax.1
        
    }
    
    public func showErrorUI(_ value: Bool) {
    }
    
}
