//
//  FormTextFieldTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 10/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import UIKit

class FormTextFieldTableViewCell: UITableViewCell, UITextFieldDelegate, FormErrorable {

    @IBOutlet weak var textField: UITextField!
    
    @IBAction func textFieldAction(_ sender: UITextField, forEvent event: UIEvent) {
        delegate?.valueChanged(in: self, with: sender.text!)
    }
    
    weak var delegate: FormTableViewCellProtocol?
    var defaultBackgroundColor: UIColor?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        textField.delegate = self
        
        defaultBackgroundColor = self.contentView.backgroundColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(with name: String, placeholder: String) {
        textField.placeholder = placeholder
        textField.text = name
    }
    
    public func showErrorUI(_ value: Bool) {
        switch value {
        case true:
            self.contentView.backgroundColor = UIColor.systemRed
        default:
            self.contentView.backgroundColor = defaultBackgroundColor
        }
    }
    
    // MARK: - UITextFieldDelegate
       
       func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           textField.resignFirstResponder()
           return true
       }
    
}
