//
//  ShowTitleTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 16/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ShowTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(with name: String) {
        titleLabel.text = name
    }
        
}
