//
//  ShowSynopsisTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 17/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ShowSynopsisTableViewCell: UITableViewCell {
    
    @IBOutlet weak var synopsisLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(with synopsis: String) {
        synopsisLabel.text = synopsis
    }
    
}
