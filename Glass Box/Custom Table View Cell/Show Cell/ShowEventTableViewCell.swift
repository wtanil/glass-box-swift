//
//  ShowEventTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 22/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ShowEventTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var eventButton: UIButton!
    
    @IBAction func eventButtonAction(_ sender: UIButton, forEvent event: UIEvent) {
        buttonAction()
    }
    
    var buttonAction: () -> Void = {}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(with eventLabel: String, buttonLabel: String, action: @escaping () -> Void) {
        
        label.text = eventLabel
        eventButton.setTitle(buttonLabel, for: .normal)
        buttonAction = action
    }
    
}
