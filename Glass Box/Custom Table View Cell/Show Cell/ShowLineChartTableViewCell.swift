//
//  ShowLineChartTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 28/05/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import Charts


class ShowLineChartTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lineChartView: ShowHistoryLineChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(with viewController: ChartViewDelegate, chartRawData: [Date: (Int, Int)]) {
        
        
//        lineChartView.configure(for: "asd", viewController: viewController)
        lineChartView.configureData(with: chartRawData)
        
        
    }
    
    func configureCell(with chartRawData: [Date: (Int, Int)]) {
        
        
//        lineChartView.configure(for: "asd", viewController: viewController)
        lineChartView.configure(for: "asd")
        lineChartView.configureData(with: chartRawData)
        
        
    }
    
    override func prepareForReuse() {
        
    }
    
}
