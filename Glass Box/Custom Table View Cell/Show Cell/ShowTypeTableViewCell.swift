//
//  ShowTypeTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 20/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ShowTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var typeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(with type: String) {
        typeLabel.text = type
    }
    
}
