//
//  ShowParameterTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 12/03/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ShowParameterTableViewCell: UITableViewCell {

    @IBOutlet weak var stackView: UIStackView!
    
    var stackViewChildren: [ParameterAttributeView] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(with data: [String: (Float, String, Date)?], sortKey: [String]) {
        
        configureAttributeSection(with: data, sortKey: sortKey)
    }
    
    private func configureAttributeSection(with data: [String: (Float, String, Date)?], sortKey: [String]) {
        
        for key in sortKey {
            if let temp = data[key], let value = temp?.0, let unit = temp?.1, let date = temp?.2 {
                
                let attributeView = ParameterAttributeView(frame: CGRect(x: 0, y: 0, width: self.stackView.frame.width, height: 50))
                
                attributeView.content = "\(key):"
                attributeView.value = "\(value) \(unit)"
                attributeView.date = DateFormatter.dateFormatterMediumMedium.string(from: date)
                
                stackView.addArrangedSubview(attributeView)
                stackViewChildren.append(attributeView)
            }
        }
    }
    
    override func prepareForReuse() {
        for childView in stackViewChildren {
            childView.removeFromSuperview()
        }
    }
    
}
