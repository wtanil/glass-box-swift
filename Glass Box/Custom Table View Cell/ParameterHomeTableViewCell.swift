//
//  ParameterHomeTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 21/02/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ParameterHomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(with parameter: Parameter) {
        timeLabel.text = DateFormatter.dateFormatterNoneShort.string(from: parameter.date!)
        titleLabel.text = parameter.name
    }
    
    public func configureCell(with title: String, time: Date) {
        timeLabel.text = DateFormatter.dateFormatterNoneShort.string(from: time)
        titleLabel.text = title
    }
    
}
