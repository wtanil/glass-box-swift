//
//  HomeMainTableViewCell.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 14/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class HomeMainTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var synopsisLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var lastUpdateDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(with name: String, description: String, date: Date, lastUpdateDate: Date) {
        nameLabel.text = name
        synopsisLabel.text = description
        dateLabel.text = "Since " + DateFormatter.dateFormatterMediumNone.string(from: date)
        lastUpdateDateLabel.text = "Last update " + DateFormatter.dateFormatterMediumShort.string(from: lastUpdateDate)
        
    }
    
    public func configureCell(with aquarium: Aquarium) {
        nameLabel.text = aquarium.name
        synopsisLabel.text = aquarium.synopsis
        dateLabel.text = "Since " + DateFormatter.dateFormatterMediumNone.string(from: aquarium.date!)
        lastUpdateDateLabel.text = "Last update " + DateFormatter.dateFormatterMediumNone.string(from: aquarium.updateDate!)
        
    }
    
}
