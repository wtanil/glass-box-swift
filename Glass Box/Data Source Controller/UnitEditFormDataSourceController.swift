//
//  UnitEditFormDataSourceController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 29/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class UnitEditFormDataSourceController {
    
    var dataSource: UITableViewDiffableDataSource<FormSectionType, UnitEditFormItem>!
    
    var sections: [FormSectionType]
    var formItems: [UnitEditFormItem]
    var cellReuseIdentifiers: [String: String]
    
    var tableView: UITableView!
    
    init(tableView: UITableView, sections: [FormSectionType], cellReuseIdentifiers: [String: String], formItems: [UnitEditFormItem]) {
        
        self.tableView = tableView
        self.sections = sections
        self.cellReuseIdentifiers = cellReuseIdentifiers
        self.formItems = formItems
        
    }
    
    func configureTableView() {
        
        for (cellIdentifier, nibName) in cellReuseIdentifiers {
            tableView.register(UINib.init(nibName: nibName, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        }
    }
    
    func apply() {
        
        var snapshot = NSDiffableDataSourceSnapshot<FormSectionType, UnitEditFormItem>()
        
        snapshot.appendSections(sections)
        
        snapshot.appendItems(formItems)
        
        dataSource.apply(snapshot, animatingDifferences: true, completion: nil)
        
    }
    
    func configureDataSource(cellProvider: @escaping UITableViewDiffableDataSource<FormSectionType, UnitEditFormItem>.CellProvider) {
        
        dataSource = UITableViewDiffableDataSource<FormSectionType, UnitEditFormItem>(tableView: tableView, cellProvider: cellProvider)
        
        dataSource.defaultRowAnimation = .fade
    }
    
}
