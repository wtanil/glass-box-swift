//
//  CoreDataDataSourceController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 27/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit
import CoreData

class CoreDataDataSourceController {
    
//    var dataSource: UITableViewDiffableDataSource<String, NSManagedObjectID>!
    var dataSource: CustomTableViewDiffableDataSource!
    
    var cellReuseIdentifiers: [String: String]
    
    var tableView: UITableView!

    init(tableView: UITableView, cellReuseIdentifiers: [String: String]) {
        self.tableView = tableView
        self.cellReuseIdentifiers = cellReuseIdentifiers
    }
    
    func configureTableView() {
        
        for (cellIdentifier, nibName) in cellReuseIdentifiers {
            tableView.register(UINib.init(nibName: nibName, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        }
    }
    
    func apply(snapshot: NSDiffableDataSourceSnapshot<String, NSManagedObjectID>, animatingDifferences: Bool) {
        
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    
    func configureDataSource(cellProvider: @escaping UITableViewDiffableDataSource<String, NSManagedObjectID>.CellProvider) {
        
//        dataSource = UITableViewDiffableDataSource<String, NSManagedObjectID>(tableView: tableView, cellProvider: cellProvider)
        dataSource = CustomTableViewDiffableDataSource(tableView: tableView, cellProvider: cellProvider)
        
        dataSource.defaultRowAnimation = .fade
    }
}
