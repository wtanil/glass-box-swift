//
//  HomeFormDataSourceController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 27/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class HomeFormDataSourceController {
    var dataSource: UITableViewDiffableDataSource<FormSectionType, HomeFormItem>!
    
    var sections: [FormSectionType]
    var formItems: [HomeFormItem]
    var cellReuseIdentifier: String
    
    var tableView: UITableView!
    
    init(tableView: UITableView, sections: [FormSectionType], cellReuseIdentifier: String, formItems: [HomeFormItem]) {
        
        self.tableView = tableView
        self.sections = sections
        self.cellReuseIdentifier = cellReuseIdentifier
        self.formItems = formItems
        
    }
    
    func configureTableView() {
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    func apply() {
        
        var snapshot = NSDiffableDataSourceSnapshot<FormSectionType, HomeFormItem>()
        snapshot.appendSections([.main])
        snapshot.appendItems(formItems)
        dataSource.apply(snapshot, animatingDifferences: true, completion: nil)
        
    }
    
    func configureDataSource(cellProvider: @escaping UITableViewDiffableDataSource<FormSectionType, HomeFormItem>.CellProvider) {
        
        dataSource = UITableViewDiffableDataSource<FormSectionType, HomeFormItem>(tableView: tableView, cellProvider: cellProvider)
        
        dataSource.defaultRowAnimation = .fade
    }
    
}
