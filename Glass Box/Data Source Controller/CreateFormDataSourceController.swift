//
//  CreateFormDataSourceController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 17/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class CreateFormDataSourceController {
    
    var dataSource: UITableViewDiffableDataSource<FormSectionType, CreateFormItem>!
    
    var sections: [FormSectionType]
    var formItems: [FormSectionType: [CreateFormItem]]
    var cellReuseIdentifiers: [String: String]
    
    var tableView: UITableView!
    
    init(tableView: UITableView, sections: [FormSectionType], cellReuseIdentifiers: [String: String], formItems: [FormSectionType: [CreateFormItem]]) {
        
        self.tableView = tableView
        self.sections = sections
        self.cellReuseIdentifiers = cellReuseIdentifiers
        self.formItems = formItems
        
    }
    
    func configureTableView() {
        
        for (cellIdentifier, nibName) in cellReuseIdentifiers {
            tableView.register(UINib.init(nibName: nibName, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        }
    }
    
    func apply() {
        
        //        let asd = append(snapshot: snapshot as NSDiffableDataSourceSnapshotReference) as NSDiffableDataSourceSnapshot<Section, CreateFormItem>
        
        //        snapshot.appendSections(withIdentifiers: sections)
        //
        //        for (section, items) in formItems {
        //            snapshot.appendItems(withIdentifiers: items, intoSectionWithIdentifier: section)
        //        }
        
        var snapshot = NSDiffableDataSourceSnapshot<FormSectionType, CreateFormItem>()
        
        snapshot.appendSections(sections)
        
        for (section, items) in formItems {
            snapshot.appendItems(items, toSection: section)
        }
        
        dataSource.apply(snapshot, animatingDifferences: true, completion: nil)
    }
    
    func configureDataSource(cellProvider: @escaping UITableViewDiffableDataSource<FormSectionType, CreateFormItem>.CellProvider) {
        
        dataSource = UITableViewDiffableDataSource<FormSectionType, CreateFormItem>(tableView: tableView, cellProvider: cellProvider)
        
        dataSource.defaultRowAnimation = .fade
    }
    
}
