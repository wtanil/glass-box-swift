//
//  ShowFormDataSourceController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 04/05/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ShowFormDataSourceController {
    
    var dataSource: UITableViewDiffableDataSource<FormSectionType, ShowFormItem>!
    
    var sections: [FormSectionType]
    var formItems: [FormSectionType: [ShowFormItem]]
    var cellReuseIdentifiers: [String: String]
    
    weak var tableView: UITableView!
    
    init(tableView: UITableView, sections: [FormSectionType], cellReuseIdentifiers: [String: String], formItems: [FormSectionType: [ShowFormItem]]) {
        
        self.tableView = tableView
        self.sections = sections
        self.cellReuseIdentifiers = cellReuseIdentifiers
        self.formItems = formItems
        
    }
    
    func configureTableView() {
        
        for (cellIdentifier, nibName) in cellReuseIdentifiers {
            tableView.register(UINib.init(nibName: nibName, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        }
    }
    
    func apply() {
        
        var snapshot = NSDiffableDataSourceSnapshot<FormSectionType, ShowFormItem>()
        
        snapshot.appendSections(sections)
        
        for (section, items) in formItems {
            snapshot.appendItems(items, toSection: section)
        }
        
        dataSource.apply(snapshot, animatingDifferences: true, completion: nil)
    }
    
    func configureDataSource(cellProvider: @escaping UITableViewDiffableDataSource<FormSectionType, ShowFormItem>.CellProvider) {
        
        dataSource = UITableViewDiffableDataSource<FormSectionType, ShowFormItem>(tableView: tableView, cellProvider: cellProvider)
        
        dataSource.defaultRowAnimation = .fade
    }
    
}

