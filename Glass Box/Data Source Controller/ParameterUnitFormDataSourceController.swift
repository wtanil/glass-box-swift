//
//  ParameterUnitFormDataSourceController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 28/04/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ParameterUnitFormDataSourceController {
    var dataSource: UITableViewDiffableDataSource<FormSectionType, ParameterUnitFormItem>!
    
    var sections: [FormSectionType]
    var formItems: [ParameterUnitFormItem]
    var cellReuseIdentifiers: [String: String]
    
    var tableView: UITableView!
    
    init(tableView: UITableView, sections: [FormSectionType], cellReuseIdentifiers: [String: String], formItems: [ParameterUnitFormItem]) {
        
        self.tableView = tableView
        self.sections = sections
        self.cellReuseIdentifiers = cellReuseIdentifiers
        self.formItems = formItems
        
    }
    
    func configureTableView() {
        
        for (cellIdentifier, nibName) in cellReuseIdentifiers {
            tableView.register(UINib.init(nibName: nibName, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        }
    }
    
    func apply() {
        
        var snapshot = NSDiffableDataSourceSnapshot<FormSectionType, ParameterUnitFormItem>()
        
        snapshot.appendSections(sections)
        
        snapshot.appendItems(formItems)
        
        dataSource.apply(snapshot, animatingDifferences: true, completion: nil)
        
    }
    
    func configureDataSource(cellProvider: @escaping UITableViewDiffableDataSource<FormSectionType, ParameterUnitFormItem>.CellProvider) {
        
        dataSource = UITableViewDiffableDataSource<FormSectionType, ParameterUnitFormItem>(tableView: tableView, cellProvider: cellProvider)
        
        dataSource.defaultRowAnimation = .fade
    }
    
}
