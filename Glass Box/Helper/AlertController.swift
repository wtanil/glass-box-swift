//
//  AlertController.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 20/01/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class AlertController: NSObject {
    
    override init() {
        super.init()
    }
    
    public func deleteAlert(with title: String, message: String, deleteHandler: ((UIAlertAction) -> Void)?, cancelHandler: ((UIAlertAction) -> Void)?) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: deleteHandler))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: cancelHandler))
        
        return alert
    }
    
    public func checkAlert(with title: String, message: String, checkHandler: ((UIAlertAction) -> Void)?, cancelHandler: ((UIAlertAction) -> Void)?) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: checkHandler))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: cancelHandler))
        
        return alert
    }

}

