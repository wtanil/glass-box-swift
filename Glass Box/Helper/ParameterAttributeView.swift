//
//  ParameterAttributeView.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 06/03/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import UIKit

class ParameterAttributeView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    var content: String? {
        get { return contentLabel?.text }
        set { contentLabel.text = newValue}
    }
    
    var date: String? {
        get { return dateLabel?.text }
        set { dateLabel.text = newValue}
    }
    
    var value: String? {
        get { return valueLabel?.text }
        set { valueLabel.text = newValue}
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    func initSubviews() {
        let nib = UINib(nibName: "ParameterAttributeView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
}
