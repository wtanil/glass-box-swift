//
//  ShowHistoryLineChartView.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 02/06/20.
//  Copyright © 2020 wtanil. All rights reserved.
//

import Foundation
import Charts

class ShowHistoryLineChartView: LineChartView {
    
    
    func configure(for type: String) {
        
//        delegate = viewController
        dragEnabled = true
        chartDescription?.enabled = false
        setScaleEnabled(true)
        pinchZoomEnabled = true
        
        
        
        leftAxis.removeAllLimitLines()
        leftAxis.valueFormatter = AxisIntValueFormatter()
        leftAxis.granularity = 1
        
        
        
        rightAxis.enabled = false
        
        xAxis.valueFormatter = AxisDateValueFormatter(dateFormat: "dd/MM")
        xAxis.labelPosition = .bottom
        
        let marker = XYMarkerView(color: UIColor(white: 180/250, alpha: 1),
                                  font: .systemFont(ofSize: 12),
                                  textColor: .white,
                                  insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                  xAxisValueFormatter: xAxis.valueFormatter!)
        marker.chartView = self
        marker.minimumSize = CGSize(width: 80, height: 40)
        self.marker = marker
        
        // prevent label to be cut off screen
        setExtraOffsets(left: 15.0, top: 15.0, right: 15.0, bottom: 15.0)
    }
    
    func configureData(with rawData: [Date: (Int, Int)]) {
        
        
        let sortKeys: [Date] = [Date](rawData.keys).sorted()
        
        var chartDataEntries: [ChartDataEntry] = []
        
        var axisMaximum: Double = 0
        var axisMinimum: Double = 0
        
        for key in sortKeys {
            let dateInDouble: Double = key.timeIntervalSince1970
            guard let value = rawData[key] else { return }
            let sumOfCount = Double(value.0 + value.1)
            let description = "Event: \(value.0)\nParameter: \(value.1)"
            let chartDataEntry = ChartDataEntry(x: dateInDouble, y: sumOfCount, data: description)
            if sumOfCount > axisMaximum { axisMaximum = sumOfCount }
//            if sumOfCount < axisMinimum { axisMinimum = sumOfCount }
            chartDataEntries.append(chartDataEntry)
        }
        
        // set number of label count
        xAxis.setLabelCount(chartDataEntries.count, force: true)
        
        leftAxis.axisMaximum = axisMaximum + 1.0
        leftAxis.axisMinimum = axisMinimum
        
        
        
        
        let dataSet: LineChartDataSet = LineChartDataSet(entries: chartDataEntries, label: "Activities")
        
        dataSet.setColor(.systemBlue)
        dataSet.setCircleColor(.systemBlue)
        dataSet.lineWidth = 3
        dataSet.circleRadius = 5
        dataSet.drawCircleHoleEnabled = false
        dataSet.valueFont = .systemFont(ofSize: 9)
//                dataSet.formLineDashLengths = [5, 2.5]
//        dataSet.formLineWidth = 5
        dataSet.formSize = 10
        
        // set value formatter for value
        let valueFormatter = NumberFormatter()
        valueFormatter.minimumFractionDigits = 0
        dataSet.valueFormatter = DefaultValueFormatter(formatter: valueFormatter)
        

        let chartData = LineChartData(dataSet: dataSet)

        self.data = chartData
        
    }
}
