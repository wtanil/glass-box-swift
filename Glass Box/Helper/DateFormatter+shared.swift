//
//  DateFormatter+shared.swift
//  Glass Box
//
//  Created by William Suryadi Tanil on 17/12/19.
//  Copyright © 2019 wtanil. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    static let dateFormatterMediumMedium: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = getPreferredLocale()
        formatter.dateStyle = .medium
        formatter.timeStyle = .medium
        return formatter
    }()
    
    static let dateFormatterMediumShort: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = getPreferredLocale()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        return formatter
    }()
    
    static let dateFormatterMediumNone: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = getPreferredLocale()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter
    }()
    
    static let dateFormatterNoneShort: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = getPreferredLocale()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter
    }()
    
    static func getPreferredLocale() -> Locale {
        guard let preferredIdentifier = Locale.preferredLanguages.first else {
            return Locale.current
        }
        return Locale(identifier: preferredIdentifier)
    }
    
    
    
}
