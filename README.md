# Glass Box
This project has been sunsetted.
Meet "Glass Box" – the ultimate mobile companion for aquarium enthusiasts. This app revolutionizes aquarium care by tracking parameters, inhabitants, and events over time. Detect imbalances early, thanks to chronological data comparisons, and effortlessly manage multiple aquariums simultaneously. Streamline tasks like updates with ease. "Glass Box" is your go-to tool for maintaining the health and harmony of your underwater world.
